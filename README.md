## Blinking Detection using Haar Cascade and A Handy Way to Diagnose Dry Eyes Disease

## ABSTRACT

Eye Blinking Detection Program, to define eye state as well as eye blinking by using Haar Cascades. Based on the result of the opened-eye detection process, eye state is determined and the eye blinking can be counted. All the results are displayed on a friendly user interface. By checking eyes state, the program deter-mines if the object is asleep or awake and activate the alert. In addition, the pro-gram also has dry-eyes disease diagnostic feature and an eye health-training pro-gram. This program is very useful for drivers by helping them stay awake or ap-ply to patients who has eye-related disease.

### Written in C++ using OpenCV

### Published in the conference of ISEE 2017 and BME 2018

### Nguyen Duy Quang - Nguyen Dinh Nguyen
* [Ho Chi Minh University of Technology](http://www.hcmut.edu.vn/)
* Program: [PFIEV](http://www.pfiev.hcmut.edu.vn/pfiev/)
* Major: Telecommunications
* Email: [nguyenduyquang06@gmail.com](nguyenduyquang06@gmail.com)
* Phone: Hidden