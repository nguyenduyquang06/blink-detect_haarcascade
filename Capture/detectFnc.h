#pragma once
#include "opencv2\opencv.hpp">
#include <opencv2\core.hpp>
#include <opencv2\highgui.hpp>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>

class detectFnc
{
public:
	detectFnc(cv::VideoCapture &capture,std::string faceCascadePath,std::string eyeCascadePath);
	~detectFnc();
	void detectFnc::captureFrame();
	void detectFace();
	void detectEye();
	std::vector<cv::Rect> faces;
	std::vector<cv::Rect> eyes;
	bool		faceFound;
	bool		eyeFound;
	cv::Mat frame;
	cv::Mat drawn;
	cv::Mat gray;
	cv::Mat grayEye;
	cv::Mat eyeROI;
	void resetVar();
private:
	cv::VideoCapture					*m_capture;
	cv::CascadeClassifier				*m_faceDetector;
	cv::CascadeClassifier				*m_eyeDetector;
	const double		scale_face = 0.2;
	const double		scale_eye = 1.2;
	const double		scaleFactor = 1.2;
	const int			minNeighbors = 2;
	const int			flagsFace = CV_HAAR_FIND_BIGGEST_OBJECT;
	const int			flagsEye = CV_HAAR_DO_CANNY_PRUNING;
	const cv::Size		minSize = cv::Size(20, 20);
	const int			deFaceW = 20;
	const int			deFaceH = 20;
	const double		eyeRectXRatio = 0.05;
	const double		eyeRectYRatio = 0.22;
	const double		eyeRectWRatio = 1-2*eyeRectXRatio;
	const double		eyeRectHRatio = 0.3;
	double				avgIntensity(cv::Mat eyeR);
	const int			fixedIntensity = 150; // Average intensity of eye frame used to detect eye
};