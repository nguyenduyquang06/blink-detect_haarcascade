#include "detectFnc.h"

detectFnc::detectFnc(cv::VideoCapture &capture,std::string faceCascadePath, std::string eyeCascadePath)
{
	m_capture = &capture;
	// Load face detector
	try {
		if (m_faceDetector == NULL) {
			m_faceDetector = new cv::CascadeClassifier(faceCascadePath);
		}
		else {
			m_faceDetector->load(faceCascadePath);
		}
	}
	catch (cv::Exception e) {}
	if (m_faceDetector->empty()) {
		fprintf(stderr, "ERROR: Could not load classifier cascade\n");
		return;
	}
	// Load eye detector
	try {
		if (m_eyeDetector == NULL) {
			m_eyeDetector = new cv::CascadeClassifier(eyeCascadePath);
		}
		else {
			m_eyeDetector->load(eyeCascadePath);
		}
	}
	catch (cv::Exception e) {}
	if (m_eyeDetector->empty()) {
		fprintf(stderr, "ERROR: Could not load classifier cascade\n");
		return;
	}
}
detectFnc::~detectFnc()
{
	if (m_faceDetector != NULL) {
		delete m_faceDetector;
	}
	if (m_eyeDetector != NULL) {
		delete m_eyeDetector;
	}
}
void detectFnc::detectFace()
{
	cv::Mat tmp;
	int resizeWidth = gray.cols * scale_face;
	int resizeHeight = gray.rows * scale_face;
	cv::resize(gray, tmp, cv::Size(resizeWidth, resizeHeight));
	m_faceDetector->detectMultiScale(tmp, faces, scaleFactor, minNeighbors, flagsFace, minSize);
	if (faces.size() > 0) {
		faceFound = true;
		for (int i = 0; i < faces.size(); i++) {
			cv::Rect smallFaceRect = faces[i];
			cv::Rect faceRect = cv::Rect(smallFaceRect.x / scale_face + deFaceW, smallFaceRect.y / scale_face + deFaceH, smallFaceRect.width / scale_face - 2 * deFaceW, smallFaceRect.height / scale_face - 2 * deFaceH);
			cv::Rect eyeRegion = cv::Rect(faceRect.x + faceRect.width*eyeRectXRatio,faceRect.y + faceRect.height*eyeRectYRatio, faceRect.width*eyeRectWRatio, faceRect.height*eyeRectHRatio);
			cv::rectangle(drawn, faceRect, cv::Scalar(0, 255,0), 2);
			cv::rectangle(drawn, eyeRegion, cv::Scalar(0, 255,0), 2);
			eyeROI = drawn(eyeRegion);
			eyeROI.copyTo(grayEye);
		}
	}
}
void detectFnc::detectEye()
{
	cv::Mat tmp;
	char buffer[100];
	int resizeWidth = grayEye.cols * scale_eye;
	int resizeHeight = grayEye.rows * scale_eye;
	cv::resize(grayEye, tmp, cv::Size(resizeWidth, resizeHeight));
	cv::cvtColor(tmp, tmp, CV_BGR2GRAY);
	double avg = avgIntensity(tmp);
	double upRatio = fixedIntensity / avg;
	tmp = tmp * upRatio;
	m_eyeDetector->detectMultiScale(tmp, eyes, scaleFactor, minNeighbors, flagsEye, minSize);
	if (eyes.size() > 0) {
		eyeFound = true;
		sprintf(buffer, "EYE OPEN");
		cv::putText(drawn, buffer, cv::Point(drawn.cols - 200, 40), 1, CV_FONT_HERSHEY_DUPLEX, cv::Scalar(255,0,0), 2);
		for (int i = 0; i < eyes.size(); i++) {
			cv::Rect smalleyeRect = eyes[i];
			cv::Rect eyeRect = cv::Rect(smalleyeRect.x / scale_eye, smalleyeRect.y / scale_eye, smalleyeRect.width / scale_eye, smalleyeRect.height / scale_eye);
			cv::rectangle(eyeROI, eyeRect, cv::Scalar(255, 0, 0),2);
		}
	}
	else {
		sprintf(buffer, "EYE CLOSE");
		cv::putText(drawn, buffer, cv::Point(drawn.cols-200, 40), 1, CV_FONT_HERSHEY_DUPLEX, cv::Scalar(0, 0, 255), 2);
	}
}
void detectFnc::captureFrame()
{
	m_capture->operator >> (frame);
	if (frame.empty()) {
		fprintf(stderr, "ERROR: Camera is not working");
	}
	cv::flip(frame, frame, 1);
	frame.copyTo(drawn);
	cv::cvtColor(frame, gray, CV_BGR2GRAY);
}
void detectFnc::resetVar()
{
	faces.clear();
	eyes.clear();
	faceFound = false;
	eyeFound = false;
	frame.release();
	drawn.release();
	gray.release();
	grayEye.release();
	eyeROI.release();
}
double detectFnc::avgIntensity(cv::Mat eyeR)
{
	int sum = 0;
	int sumVal = 0;
	double avg;
	for (int i = 0; i < eyeR.rows; i++)
	{
		for (int j = 0; j < eyeR.cols; j++)
		{
			uchar val = eyeR.at<uchar>(i, j);
			int intVal = (int)val;
			sum = sum + 1;
			sumVal = sumVal + intVal;
			avg = sumVal / sum;
		}
	}
	return avg;
}