#pragma once
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <time.h>
#include <ctype.h>
#include <windows.h>
#include <mmsystem.h>
#include "detectFnc.h"
#pragma comment(lib, "winmm.lib")

class blinkCounter
{
public:
	blinkCounter(detectFnc &detector);
	~blinkCounter();
	void addText();
	void toogleDispFPS();
	void toogleDispTime();
	void toogleDispCounter();
	void toogleAlarm();
	void run();
	long				fps;
	cv::Mat				result;
private:
	detectFnc			*m_detector;
	long				frameCounter;
	int					tick;
	time_t				start,dur,rawtime;
	bool				dispFPS, dispTime;
	int blinkCount = 0;
	bool eyeOpen = true;
	bool counting = false;
	int				initation, fin;
	int				warningEnd = 0;
	int				voiceDuration = 2;
	int				warningTime = 3;
	bool			alarm = false;
};