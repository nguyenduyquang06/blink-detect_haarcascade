﻿#pragma once
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <time.h>
#include <ctype.h>
#include <windows.h>
#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")
using namespace std;
using namespace cv;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Runtime::InteropServices;

namespace MainForm {
	public ref class Form1 : public System::Windows::Forms::Form {
	public:
		Form1(void) {
			InitializeComponent();
		}
	protected:
		~Form1() {
			if (components)	{
				delete components;
			}
		}
#pragma region Window_control
	private: 
		System::ComponentModel::IContainer^  components;
#pragma endregion

#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			// Gcnew
			this->components = (gcnew System::ComponentModel::Container());
			// Form
			this->Size = System::Drawing::Size(200, 200);
			// Add
			// EventHandler
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
		}
#pragma endregion

#pragma region EventHandler
		System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
			char buffer[100];
			sprintf(buffer, "asd");
		}
#pragma endregion
	};
}