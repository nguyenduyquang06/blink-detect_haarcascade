﻿#pragma once
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <time.h>
#include <ctype.h>
#include <windows.h>
#include <mmsystem.h>
#include <iostream>
#include <fstream>
#include <string.h>
#pragma comment(lib, "winmm.lib")
using namespace std;
using namespace cv;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Runtime::InteropServices;

const string faceCascadePath = "haarcascade_frontalface_alt2.xml";
const string eyeCascadePath = "haarcascade_eye_tree_eyeglasses.xml";
VideoCapture capture;

// Detect Fnc vars and fncs
void captureFrame();
void detectFace();
void detectEye();
std::vector<cv::Rect> faces;
std::vector<cv::Rect> eyes;
bool		faceFound;
bool		eyeFound;
cv::Mat frame;
cv::Mat drawn;
cv::Mat gray;
cv::Mat grayEye;
cv::Mat eyeROI;
void resetVar();
cv::VideoCapture					*m_capture;
cv::CascadeClassifier				*m_faceDetector;
cv::CascadeClassifier				*m_eyeDetector;
double		scale_face = 0.2;
double		scale_eye = 1;
const double		scaleFactor = 1.2;
const int			minNeighbors = 2;
const int			flagsFace = CV_HAAR_FIND_BIGGEST_OBJECT;
const int			flagsEye = CV_HAAR_DO_CANNY_PRUNING;
const cv::Size		minSize = cv::Size(20, 20);
const int			deFaceW = 20;
const int			deFaceH = 20;
const double		eyeRectXRatio = 0.05;
const double		eyeRectYRatio = 0.22;
const double		eyeRectWRatio = 1 - 2 * eyeRectXRatio;
const double		eyeRectHRatio = 0.3;
double				avgIntensity(cv::Mat eyeR);
const int			fixedIntensity = 180;
// blink Counter
long				fps;
cv::Mat				result;
long				frameCounter;
int					tick;
time_t				start, dur, rawtime;
int blinkCount = 0;
bool eyePrevStt = false;
bool facePrevStt = false;
bool counting = false;
int				beginSleep, endSleep;
int				beginWake, endWake;
int				sleepWarningEnd = 0;
int				sleepAlarmVoiceDuration = 2;
bool			ringWakeAlarm = false;
int				warningSleepTime = 3;
int				warningWakeTime = 3;
bool			sleepState = false;
bool			wakeState = true;
bool			SleepAlarm = false;
bool			WakeAlarm = false;
string			realTime;
// User Form
char			*userName;
int				userAge = 0;
bool			userMale = false;
char			*userDate;
char			*dataFilePath = NULL;
bool			userSession = false;
bool			fileOpened = false;
std::ofstream	tempFile;
// Upgrade part
void separateHist(cv::Mat &faceImg);
clock_t lidClose, lidOpen;
double closureTime;
double diffclock(clock_t clock1, clock_t clock2);
int allLidClosure = 0, extendLidClosure = 0, superExtendLidClosure = 0;
double allLidClosureTotalTime = 0, extendLidClosureTotalTime = 0, superExtendClosureTotalTime = 0;
clock_t recordStart;
bool record = false;
double avgA, avgE, avgSE, a, ex, se;
double avgAnorm = 0.7, avgEnorm = 0.4, avgSEnorm = 0.2;
double aNorm = 0.08, eNorm = 0.575, seNorm = 1.589;

// Auto define detect scale
bool firstDetect = false;
bool autoMode = false;
double saveFS, saveES;
int setFirstDetect = 0;
int FRW = 0;
int fixedEyeH = 100;
int fixedFaceH = 40;
namespace Capture1 {
	public ref class DiagnoseForm : public System::Windows::Forms::Form
	{
	public:
		DiagnoseForm(void)
		{
			InitializeComponent();
		}
		~DiagnoseForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private:
		System::ComponentModel::IContainer^  components;
		System::Windows::Forms::Button^ buttonSet;
		System::Windows::Forms::Button^ buttonReset;
		System::Windows::Forms::Label^ labelResult;
		System::Windows::Forms::Button^ buttonDiagnose;

		System::Windows::Forms::GroupBox^ groupBoxPatientP;
		System::Windows::Forms::CheckBox^ checkBoxManual;
		System::Windows::Forms::Label^ labelPatientACT;
		System::Windows::Forms::Label^ labelPatientCPM;
		System::Windows::Forms::Label^ labelPatientACTA;
		System::Windows::Forms::Label^ labelPatientACTE;
		System::Windows::Forms::Label^ labelPatientACTSE;
		System::Windows::Forms::Label^ labelPatientCPMA;
		System::Windows::Forms::Label^ labelPatientCPME;
		System::Windows::Forms::Label^ labelPatientCPMSE;
		System::Windows::Forms::TextBox^ textBoxPatientACTA;
		System::Windows::Forms::TextBox^ textBoxPatientACTE;
		System::Windows::Forms::TextBox^ textBoxPatientACTSE;
		System::Windows::Forms::TextBox^ textBoxPatientCPMA;
		System::Windows::Forms::TextBox^ textBoxPatientCPME;
		System::Windows::Forms::TextBox^ textBoxPatientCPMSE;

		System::Windows::Forms::GroupBox^ groupBoxNormalP;
		System::Windows::Forms::Label^ labelNormalACT;
		System::Windows::Forms::Label^ labelNormalCPM;
		System::Windows::Forms::Label^ labelNormalACTA;
		System::Windows::Forms::Label^ labelNormalACTE;
		System::Windows::Forms::Label^ labelNormalACTSE;
		System::Windows::Forms::Label^ labelNormalCPMA;
		System::Windows::Forms::Label^ labelNormalCPME;
		System::Windows::Forms::Label^ labelNormalCPMSE;
		System::Windows::Forms::TextBox^ textBoxNormalACTA;
		System::Windows::Forms::TextBox^ textBoxNormalACTE;
		System::Windows::Forms::TextBox^ textBoxNormalACTSE;
		System::Windows::Forms::TextBox^ textBoxNormalCPMA;
		System::Windows::Forms::TextBox^ textBoxNormalCPME;
		System::Windows::Forms::TextBox^ textBoxNormalCPMSE;
		void InitializeComponent(void)
		{
			// Create object
			this->components = gcnew System::ComponentModel::Container();
			this->buttonSet = gcnew System::Windows::Forms::Button();
			this->buttonReset = gcnew System::Windows::Forms::Button();
			this->labelResult = gcnew System::Windows::Forms::Label();
			this->buttonDiagnose = gcnew System::Windows::Forms::Button();

			this->groupBoxPatientP = gcnew System::Windows::Forms::GroupBox();
			this->checkBoxManual = gcnew System::Windows::Forms::CheckBox();
			this->labelPatientACT = gcnew System::Windows::Forms::Label();
			this->labelPatientCPM = gcnew System::Windows::Forms::Label();
			this->labelPatientACTA = gcnew System::Windows::Forms::Label();
			this->labelPatientACTE = gcnew System::Windows::Forms::Label();
			this->labelPatientACTSE = gcnew System::Windows::Forms::Label();
			this->labelPatientCPMA = gcnew System::Windows::Forms::Label();
			this->labelPatientCPME = gcnew System::Windows::Forms::Label();
			this->labelPatientCPMSE = gcnew System::Windows::Forms::Label();
			this->textBoxPatientACTA = gcnew System::Windows::Forms::TextBox();
			this->textBoxPatientACTE = gcnew System::Windows::Forms::TextBox();
			this->textBoxPatientACTSE = gcnew System::Windows::Forms::TextBox();
			this->textBoxPatientCPMA = gcnew System::Windows::Forms::TextBox();
			this->textBoxPatientCPME = gcnew System::Windows::Forms::TextBox();
			this->textBoxPatientCPMSE = gcnew System::Windows::Forms::TextBox();

			this->groupBoxNormalP = gcnew System::Windows::Forms::GroupBox();
			this->labelNormalACT = gcnew System::Windows::Forms::Label();
			this->labelNormalCPM = gcnew System::Windows::Forms::Label();
			this->labelNormalACTA = gcnew System::Windows::Forms::Label();
			this->labelNormalACTE = gcnew System::Windows::Forms::Label();
			this->labelNormalACTSE = gcnew System::Windows::Forms::Label();
			this->labelNormalCPMA = gcnew System::Windows::Forms::Label();
			this->labelNormalCPME = gcnew System::Windows::Forms::Label();
			this->labelNormalCPMSE = gcnew System::Windows::Forms::Label();
			this->textBoxNormalACTA = gcnew System::Windows::Forms::TextBox();
			this->textBoxNormalACTE = gcnew System::Windows::Forms::TextBox();
			this->textBoxNormalACTSE = gcnew System::Windows::Forms::TextBox();
			this->textBoxNormalCPMA = gcnew System::Windows::Forms::TextBox();
			this->textBoxNormalCPME = gcnew System::Windows::Forms::TextBox();
			this->textBoxNormalCPMSE = gcnew System::Windows::Forms::TextBox();
			//Properties
			//Form
			this->ClientSize = System::Drawing::Size(446, 260);
			this->Font = (gcnew System::Drawing::Font(L"Calibri", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"DiagnoseForm";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Diagnose";
			this->Show();

#pragma region Parameters group box
			// Group Box PatientP
			this->Controls->Add(this->groupBoxPatientP);
			this->groupBoxPatientP->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->groupBoxPatientP->Location = System::Drawing::Point(3, 3);
			this->groupBoxPatientP->Name = L"groupBoxPatientP";
			this->groupBoxPatientP->Size = System::Drawing::Size(443, 85);
			this->groupBoxPatientP->TabIndex = 10;
			this->groupBoxPatientP->TabStop = false;
			this->groupBoxPatientP->Text = L"Patient parameters";

			//Checkbox manual configuration
			this->groupBoxPatientP->Controls->Add(this->checkBoxManual);
			this->checkBoxManual->Size = System::Drawing::Size(70, 25);
			this->checkBoxManual->Location = System::Drawing::Point(380, 0);
			this->checkBoxManual->Font = (gcnew System::Drawing::Font(L"Calibri", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->checkBoxManual->Text = "Manual";
			this->checkBoxManual->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->checkBoxManual->Checked = false;
			this->checkBoxManual->CheckStateChanged += gcnew System::EventHandler(this, &DiagnoseForm::checkBoxManual_checkStateChanged);


			// Label PACT
			this->groupBoxPatientP->Controls->Add(this->labelPatientACT);
			this->labelPatientACT->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelPatientACT->Size = System::Drawing::Size(150, 25);
			this->labelPatientACT->Location = System::Drawing::Point(3, 25);
			this->labelPatientACT->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelPatientACT->Text = "Average closure time";
			this->labelPatientACT->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;

			// Label PatientCPM
			this->groupBoxPatientP->Controls->Add(this->labelPatientCPM);
			this->labelPatientCPM->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelPatientCPM->Size = System::Drawing::Size(150, 25);
			this->labelPatientCPM->Location = System::Drawing::Point(3, 50);
			this->labelPatientCPM->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelPatientCPM->Text = "The contact rate";
			this->labelPatientCPM->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;

			// Label PACTA
			this->groupBoxPatientP->Controls->Add(this->labelPatientACTA);
			this->labelPatientACTA->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelPatientACTA->Size = System::Drawing::Size(20, 25);
			this->labelPatientACTA->Location = System::Drawing::Point(158, 25);
			this->labelPatientACTA->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelPatientACTA->Text = "A";
			this->labelPatientACTA->TextAlign = System::Drawing::ContentAlignment::MiddleRight;

			// Textbox PatientACTA
			this->groupBoxPatientP->Controls->Add(this->textBoxPatientACTA);
			this->textBoxPatientACTA->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->textBoxPatientACTA->Size = System::Drawing::Size(70, 25);
			this->textBoxPatientACTA->Location = System::Drawing::Point(180, 25);
			this->textBoxPatientACTA->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxPatientACTA->Text = "";
			this->textBoxPatientACTA->ReadOnly = true;

			// Label PACTE
			this->groupBoxPatientP->Controls->Add(this->labelPatientACTE);
			this->labelPatientACTE->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelPatientACTE->Size = System::Drawing::Size(20, 25);
			this->labelPatientACTE->Location = System::Drawing::Point(253, 25);
			this->labelPatientACTE->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelPatientACTE->Text = "E";
			this->labelPatientACTE->TextAlign = System::Drawing::ContentAlignment::MiddleRight;

			// Textbox PatientACTE
			this->groupBoxPatientP->Controls->Add(this->textBoxPatientACTE);
			this->textBoxPatientACTE->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->textBoxPatientACTE->Size = System::Drawing::Size(70, 25);
			this->textBoxPatientACTE->Location = System::Drawing::Point(275, 25);
			this->textBoxPatientACTE->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxPatientACTE->Text = "";
			this->textBoxPatientACTE->ReadOnly = true;

			// Label PACTSE
			this->groupBoxPatientP->Controls->Add(this->labelPatientACTSE);
			this->labelPatientACTSE->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelPatientACTSE->Size = System::Drawing::Size(20, 25);
			this->labelPatientACTSE->Location = System::Drawing::Point(348, 25);
			this->labelPatientACTSE->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelPatientACTSE->Text = "S";
			this->labelPatientACTSE->TextAlign = System::Drawing::ContentAlignment::MiddleRight;

			// Textbox PatientACTSE
			this->groupBoxPatientP->Controls->Add(this->textBoxPatientACTSE);
			this->textBoxPatientACTSE->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->textBoxPatientACTSE->Size = System::Drawing::Size(70, 25);
			this->textBoxPatientACTSE->Location = System::Drawing::Point(370, 25);
			this->textBoxPatientACTSE->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxPatientACTSE->Text = "";
			this->textBoxPatientACTSE->ReadOnly = true;

			// Label PCPMA
			this->groupBoxPatientP->Controls->Add(this->labelPatientCPMA);
			this->labelPatientCPMA->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelPatientCPMA->Size = System::Drawing::Size(20, 25);
			this->labelPatientCPMA->Location = System::Drawing::Point(158, 50);
			this->labelPatientCPMA->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelPatientCPMA->Text = "A";
			this->labelPatientCPMA->TextAlign = System::Drawing::ContentAlignment::MiddleRight;

			// Textbox PatientCPMA
			this->groupBoxPatientP->Controls->Add(this->textBoxPatientCPMA);
			this->textBoxPatientCPMA->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->textBoxPatientCPMA->Size = System::Drawing::Size(70, 25);
			this->textBoxPatientCPMA->Location = System::Drawing::Point(180, 50);
			this->textBoxPatientCPMA->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxPatientCPMA->Text = "";
			this->textBoxPatientCPMA->ReadOnly = true;

			// Label PCPME
			this->groupBoxPatientP->Controls->Add(this->labelPatientCPME);
			this->labelPatientCPME->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelPatientCPME->Size = System::Drawing::Size(20, 25);
			this->labelPatientCPME->Location = System::Drawing::Point(253, 50);
			this->labelPatientCPME->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelPatientCPME->Text = "E";
			this->labelPatientCPME->TextAlign = System::Drawing::ContentAlignment::MiddleRight;

			// Textbox PatientCPME
			this->groupBoxPatientP->Controls->Add(this->textBoxPatientCPME);
			this->textBoxPatientCPME->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->textBoxPatientCPME->Size = System::Drawing::Size(70, 25);
			this->textBoxPatientCPME->Location = System::Drawing::Point(275, 50);
			this->textBoxPatientCPME->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxPatientCPME->Text = "";
			this->textBoxPatientCPME->ReadOnly = true;

			// Label PCPMSE
			this->groupBoxPatientP->Controls->Add(this->labelPatientCPMSE);
			this->labelPatientCPMSE->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelPatientCPMSE->Size = System::Drawing::Size(20, 25);
			this->labelPatientCPMSE->Location = System::Drawing::Point(348, 50);
			this->labelPatientCPMSE->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelPatientCPMSE->Text = "S";
			this->labelPatientCPMSE->TextAlign = System::Drawing::ContentAlignment::MiddleRight;

			// Textbox PatientCPMSE
			this->groupBoxPatientP->Controls->Add(this->textBoxPatientCPMSE);
			this->textBoxPatientCPMSE->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->textBoxPatientCPMSE->Size = System::Drawing::Size(70, 25);
			this->textBoxPatientCPMSE->Location = System::Drawing::Point(370, 50);
			this->textBoxPatientCPMSE->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxPatientCPMSE->Text = "";
			this->textBoxPatientCPMSE->ReadOnly = true;
#pragma endregion

#pragma region Normal parameters
			// Group Box NormalP
			this->Controls->Add(this->groupBoxNormalP);
			this->groupBoxNormalP->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->groupBoxNormalP->Location = System::Drawing::Point(3, 95);
			this->groupBoxNormalP->Name = L"groupBoxNormalP";
			this->groupBoxNormalP->Size = System::Drawing::Size(443, 85);
			this->groupBoxNormalP->TabIndex = 10;
			this->groupBoxNormalP->TabStop = false;
			this->groupBoxNormalP->Text = L"Standard parameters";

			// Label PACT
			this->groupBoxNormalP->Controls->Add(this->labelNormalACT);
			this->labelNormalACT->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelNormalACT->Size = System::Drawing::Size(150, 25);
			this->labelNormalACT->Location = System::Drawing::Point(3, 25);
			this->labelNormalACT->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelNormalACT->Text = "Average closure time";
			this->labelNormalACT->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;

			// Label NormalCPM
			this->groupBoxNormalP->Controls->Add(this->labelNormalCPM);
			this->labelNormalCPM->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelNormalCPM->Size = System::Drawing::Size(150, 25);
			this->labelNormalCPM->Location = System::Drawing::Point(3, 50);
			this->labelNormalCPM->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelNormalCPM->Text = "The contact rate";
			this->labelNormalCPM->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;

			// Label PACTA
			this->groupBoxNormalP->Controls->Add(this->labelNormalACTA);
			this->labelNormalACTA->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelNormalACTA->Size = System::Drawing::Size(20, 25);
			this->labelNormalACTA->Location = System::Drawing::Point(158, 25);
			this->labelNormalACTA->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelNormalACTA->Text = "A";
			this->labelNormalACTA->TextAlign = System::Drawing::ContentAlignment::MiddleRight;

			// Textbox NormalACTA
			this->groupBoxNormalP->Controls->Add(this->textBoxNormalACTA);
			this->textBoxNormalACTA->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->textBoxNormalACTA->Size = System::Drawing::Size(70, 25);
			this->textBoxNormalACTA->Location = System::Drawing::Point(180, 25);
			this->textBoxNormalACTA->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxNormalACTA->Text = "" + aNorm;

			// Label PACTE
			this->groupBoxNormalP->Controls->Add(this->labelNormalACTE);
			this->labelNormalACTE->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelNormalACTE->Size = System::Drawing::Size(20, 25);
			this->labelNormalACTE->Location = System::Drawing::Point(253, 25);
			this->labelNormalACTE->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelNormalACTE->Text = "E";
			this->labelNormalACTE->TextAlign = System::Drawing::ContentAlignment::MiddleRight;

			// Textbox NormalACTE
			this->groupBoxNormalP->Controls->Add(this->textBoxNormalACTE);
			this->textBoxNormalACTE->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->textBoxNormalACTE->Size = System::Drawing::Size(70, 25);
			this->textBoxNormalACTE->Location = System::Drawing::Point(275, 25);
			this->textBoxNormalACTE->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxNormalACTE->Text = "" + eNorm;

			// Label PACTSE
			this->groupBoxNormalP->Controls->Add(this->labelNormalACTSE);
			this->labelNormalACTSE->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelNormalACTSE->Size = System::Drawing::Size(20, 25);
			this->labelNormalACTSE->Location = System::Drawing::Point(348, 25);
			this->labelNormalACTSE->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelNormalACTSE->Text = "S";
			this->labelNormalACTSE->TextAlign = System::Drawing::ContentAlignment::MiddleRight;

			// Textbox NormalACTSE
			this->groupBoxNormalP->Controls->Add(this->textBoxNormalACTSE);
			this->textBoxNormalACTSE->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->textBoxNormalACTSE->Size = System::Drawing::Size(70, 25);
			this->textBoxNormalACTSE->Location = System::Drawing::Point(370, 25);
			this->textBoxNormalACTSE->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxNormalACTSE->Text = "" + seNorm;

			// Label PCPMA
			this->groupBoxNormalP->Controls->Add(this->labelNormalCPMA);
			this->labelNormalCPMA->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelNormalCPMA->Size = System::Drawing::Size(20, 25);
			this->labelNormalCPMA->Location = System::Drawing::Point(158, 50);
			this->labelNormalCPMA->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelNormalCPMA->Text = "A";
			this->labelNormalCPMA->TextAlign = System::Drawing::ContentAlignment::MiddleRight;

			// Textbox NormalCPMA
			this->groupBoxNormalP->Controls->Add(this->textBoxNormalCPMA);
			this->textBoxNormalCPMA->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->textBoxNormalCPMA->Size = System::Drawing::Size(70, 25);
			this->textBoxNormalCPMA->Location = System::Drawing::Point(180, 50);
			this->textBoxNormalCPMA->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxNormalCPMA->Text = "" + avgAnorm;

			// Label PCPME
			this->groupBoxNormalP->Controls->Add(this->labelNormalCPME);
			this->labelNormalCPME->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelNormalCPME->Size = System::Drawing::Size(20, 25);
			this->labelNormalCPME->Location = System::Drawing::Point(253, 50);
			this->labelNormalCPME->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelNormalCPME->Text = "E";
			this->labelNormalCPME->TextAlign = System::Drawing::ContentAlignment::MiddleRight;

			// Textbox NormalCPME
			this->groupBoxNormalP->Controls->Add(this->textBoxNormalCPME);
			this->textBoxNormalCPME->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->textBoxNormalCPME->Size = System::Drawing::Size(70, 25);
			this->textBoxNormalCPME->Location = System::Drawing::Point(275, 50);
			this->textBoxNormalCPME->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxNormalCPME->Text = "" + avgEnorm;

			// Label PCPMSE
			this->groupBoxNormalP->Controls->Add(this->labelNormalCPMSE);
			this->labelNormalCPMSE->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelNormalCPMSE->Size = System::Drawing::Size(20, 25);
			this->labelNormalCPMSE->Location = System::Drawing::Point(348, 50);
			this->labelNormalCPMSE->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelNormalCPMSE->Text = "S";
			this->labelNormalCPMSE->TextAlign = System::Drawing::ContentAlignment::MiddleRight;

			// Textbox NormalCPMSE
			this->groupBoxNormalP->Controls->Add(this->textBoxNormalCPMSE);
			this->textBoxNormalCPMSE->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->textBoxNormalCPMSE->Size = System::Drawing::Size(70, 25);
			this->textBoxNormalCPMSE->Location = System::Drawing::Point(370, 50);
			this->textBoxNormalCPMSE->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxNormalCPMSE->Text = "" + avgSEnorm;
#pragma endregion



			//Button Set
			this->Controls->Add(this->buttonSet);
			this->buttonSet->Font = gcnew System::Drawing::Font(L"Calibri", 12);
			this->buttonSet->Location = System::Drawing::Point(25, 190);
			this->buttonSet->Name = L"buttonSet";
			this->buttonSet->Size = System::Drawing::Size(70, 25);
			this->buttonSet->TabIndex = 12;
			this->buttonSet->Text = L"Set";
			this->buttonSet->UseVisualStyleBackColor = true;
			this->buttonSet->Click += gcnew System::EventHandler(this, &DiagnoseForm::buttonSet_Click);

			//Button Reset
			this->Controls->Add(this->buttonReset);
			this->buttonReset->Font = gcnew System::Drawing::Font(L"Calibri", 12);
			this->buttonReset->Location = System::Drawing::Point(25, 225);
			this->buttonReset->Name = L"buttonReset";
			this->buttonReset->Size = System::Drawing::Size(70, 25);
			this->buttonReset->TabIndex = 12;
			this->buttonReset->Text = L"Reset";
			this->buttonReset->UseVisualStyleBackColor = true;
			this->buttonReset->Click += gcnew System::EventHandler(this, &DiagnoseForm::buttonReset_Click);

			// Label Result
			this->Controls->Add(this->labelResult);
			this->labelResult->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->labelResult->Size = System::Drawing::Size(250, 30);
			this->labelResult->Location = System::Drawing::Point(130, 220);
			this->labelResult->Font = (gcnew System::Drawing::Font(L"Calibri", 15, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelResult->Text = "Record data to start";
			this->labelResult->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;

			//Button Diagnose
			this->Controls->Add(this->buttonDiagnose);
			this->buttonDiagnose->Font = gcnew System::Drawing::Font(L"Calibri", 12);
			this->buttonDiagnose->Location = System::Drawing::Point(150, 190);
			this->buttonDiagnose->Name = L"buttonDiagnose";
			this->buttonDiagnose->Size = System::Drawing::Size(100, 25);
			this->buttonDiagnose->TabIndex = 12;
			this->buttonDiagnose->Text = L"Calculate";
			this->buttonDiagnose->UseVisualStyleBackColor = true;
			this->buttonDiagnose->Click += gcnew System::EventHandler(this, &DiagnoseForm::buttonDiagnose_Click);

		}
		System::Void checkBoxManual_checkStateChanged(System::Object^ sender, System::EventArgs^ e) {
			if (checkBoxManual->Checked) {
				this->textBoxPatientACTA->ReadOnly = false;
				this->textBoxPatientACTE->ReadOnly = false;
				this->textBoxPatientACTSE->ReadOnly = false;
				this->textBoxPatientCPMA->ReadOnly = false;
				this->textBoxPatientCPME->ReadOnly = false;
				this->textBoxPatientCPMSE->ReadOnly = false;
			}
			else {
				this->textBoxPatientACTA->ReadOnly = true;
				this->textBoxPatientACTE->ReadOnly = true;
				this->textBoxPatientACTSE->ReadOnly = true;
				this->textBoxPatientCPMA->ReadOnly = true;
				this->textBoxPatientCPME->ReadOnly = true;
				this->textBoxPatientCPMSE->ReadOnly = true;
			}
		}
		System::Void buttonSet_Click(System::Object^ sender, System::EventArgs^ e) {
			avgAnorm = Double::Parse(this->textBoxNormalCPMA->Text);
			avgEnorm = Double::Parse(this->textBoxNormalCPME->Text);
			avgSEnorm = Double::Parse(this->textBoxNormalCPMSE->Text);
			aNorm = Double::Parse(this->textBoxNormalACTA->Text);
			eNorm = Double::Parse(this->textBoxNormalACTE->Text);
			seNorm = Double::Parse(this->textBoxNormalACTSE->Text);
		}
		System::Void buttonReset_Click(System::Object^ sender, System::EventArgs^ e) {
			avgAnorm = 0.7;
			avgEnorm = 0.4;
			avgSEnorm = 0.2;
			aNorm = 0.08;
			eNorm = 0.575;
			seNorm = 1.589;
			this->textBoxNormalACTA->Text = "" + aNorm;
			this->textBoxNormalACTE->Text = "" + eNorm;
			this->textBoxNormalACTSE->Text = "" + seNorm;
			this->textBoxNormalCPMA->Text = "" + avgAnorm;
			this->textBoxNormalCPME->Text = "" + avgEnorm;
			this->textBoxNormalCPMSE->Text = "" + avgSEnorm;
		}
		System::Void buttonDiagnose_Click(System::Object^ sender, System::EventArgs^ e) {

			if (this->checkBoxManual->Checked == true) {
				a = this->textBoxPatientACTA->Text->Length == 0 ? 0 : Double::Parse(this->textBoxPatientACTA->Text);
				ex = this->textBoxPatientACTE->Text->Length == 0 ? 0 : Double::Parse(this->textBoxPatientACTE->Text);
				se = this->textBoxPatientACTSE->Text->Length == 0 ? 0 : Double::Parse(this->textBoxPatientACTSE->Text);
				avgA = this->textBoxPatientCPMA->Text->Length == 0 ? 0 : Double::Parse(this->textBoxPatientCPMA->Text);
				avgE = this->textBoxPatientCPME->Text->Length == 0 ? 0 : Double::Parse(this->textBoxPatientCPME->Text);
				avgSE = this->textBoxPatientCPMSE->Text->Length == 0 ? 0 : Double::Parse(this->textBoxPatientCPMSE->Text);
			}
			this->textBoxPatientACTA->Text = "" + a;
			this->textBoxPatientACTE->Text = "" + ex;
			this->textBoxPatientACTSE->Text = "" + se;
			this->textBoxPatientCPMA->Text = "" + avgA;
			this->textBoxPatientCPME->Text = "" + avgE;
			this->textBoxPatientCPMSE->Text = "" + avgSE;
			int dryeyes = 0;
			double ratioCPMA = avgA / avgAnorm;
			double ratioCPME = avgE / avgEnorm;
			double ratioCPMSE = avgSE / avgSEnorm;
			double ratioACTA = a / aNorm;
			double ratioACTE = ex / eNorm;
			double ratioACTSE = se / seNorm;
			if (ratioCPMA > 2.47 && ratioCPMA < 16.46) dryeyes++;
			if (ratioCPME > 3.35 && ratioCPME < 47.26) dryeyes++;
			if (ratioCPMSE > 17.73 && ratioCPMSE < 113.16) dryeyes++;
			if (ratioACTA > 2.46 && ratioACTA < 20.33) dryeyes++;
			if (ratioACTE > 1.65 && ratioACTE < 11.07) dryeyes++;
			if (ratioACTSE > 3.28 && ratioACTSE < 6.15) dryeyes++;
			if (dryeyes < 2) this->labelResult->Text = "Non-dry eyes";
			else if (dryeyes < 5) this->labelResult->Text = "Dry eyes potential";
			else if (dryeyes <= 6) this->labelResult->Text = "Dry eyes";
		}
	};
	public ref class UserForm : public System::Windows::Forms::Form
	{
	private:
		char		*fileName;
	public:
		UserForm(void)
		{
			InitializeComponent();
		}
		~UserForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private:
		System::ComponentModel::IContainer^  components;
		System::Windows::Forms::Label^ labelTitle;
		System::Windows::Forms::Label^ labelName;
		System::Windows::Forms::Label^ labelDate;
		System::Windows::Forms::Label^ labelAge;
		System::Windows::Forms::CheckBox^ checkBoxMale;
		System::Windows::Forms::Label^ labelDataFilePath;
		System::Windows::Forms::TextBox^ textBoxName;
		System::Windows::Forms::TextBox^ textBoxAge;
		System::Windows::Forms::DateTimePicker^ dateTimePicker;
		System::Windows::Forms::TextBox^ textBoxDataFilePath;
		System::Windows::Forms::Button^ buttonBrowse;
		System::Windows::Forms::OpenFileDialog^  openFileDialog1;
		System::Windows::Forms::Button^ buttonConfirm;
		void InitializeComponent(void)
		{
			// Create object
			this->components = (gcnew System::ComponentModel::Container());
			this->labelTitle = gcnew System::Windows::Forms::Label();
			this->labelName = gcnew System::Windows::Forms::Label();
			this->labelDate = gcnew System::Windows::Forms::Label();
			this->labelAge = gcnew System::Windows::Forms::Label();
			this->labelDataFilePath = gcnew System::Windows::Forms::Label();
			this->checkBoxMale = gcnew System::Windows::Forms::CheckBox();
			this->textBoxName = gcnew System::Windows::Forms::TextBox();
			this->textBoxAge = gcnew System::Windows::Forms::TextBox();
			this->dateTimePicker = gcnew System::Windows::Forms::DateTimePicker();
			this->textBoxDataFilePath = gcnew System::Windows::Forms::TextBox();
			this->buttonBrowse = gcnew System::Windows::Forms::Button();
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->buttonConfirm = gcnew System::Windows::Forms::Button();
			// Add
			this->Controls->Add(this->labelTitle);
			this->Controls->Add(this->labelName);
			this->Controls->Add(this->labelAge);
			this->Controls->Add(this->labelDate);
			this->Controls->Add(this->labelDataFilePath);
			this->Controls->Add(this->checkBoxMale);
			this->Controls->Add(this->textBoxName);
			this->Controls->Add(this->textBoxAge);
			this->Controls->Add(this->dateTimePicker);
			this->Controls->Add(this->textBoxDataFilePath);
			this->Controls->Add(this->buttonBrowse);
			this->Controls->Add(this->buttonConfirm);
			//Properties
			//Form
			this->ClientSize = System::Drawing::Size(250, 260);
			this->Font = (gcnew System::Drawing::Font(L"Calibri", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"UserForm";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
			this->Text = L"User Data Insert Form";
			this->Show();

			// Button Confirm
			this->buttonConfirm->Font = gcnew System::Drawing::Font(L"Calibri", 13);
			this->buttonConfirm->Size = System::Drawing::Size(100, 30);
			this->buttonConfirm->Location = System::Drawing::Point((250 - buttonConfirm->Size.Width) / 2, 215);
			this->buttonConfirm->Name = L"buttonConfirm";
			this->buttonConfirm->TabIndex = 12;
			this->buttonConfirm->Text = L"Confirm";
			this->buttonConfirm->UseVisualStyleBackColor = true;
			this->buttonConfirm->Click += gcnew System::EventHandler(this, &UserForm::buttonConfirm_Click);

			// Button Browse
			this->buttonBrowse->Font = gcnew System::Drawing::Font(L"Calibri", 13);
			this->buttonBrowse->Location = System::Drawing::Point(172, 149);
			this->buttonBrowse->Name = L"buttonBrowse";
			this->buttonBrowse->Size = System::Drawing::Size(71, 30);
			this->buttonBrowse->TabIndex = 12;
			this->buttonBrowse->Text = L"Open";
			this->buttonBrowse->UseVisualStyleBackColor = true;
			this->buttonBrowse->Click += gcnew System::EventHandler(this, &UserForm::buttonBrowse_Click);

			// Textbox Name
			this->textBoxName->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->textBoxName->Size = System::Drawing::Size(171, 30);
			this->textBoxName->Location = System::Drawing::Point(72, 44);
			this->textBoxName->Font = (gcnew System::Drawing::Font(L"Calibri", 13, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxName->Text = "";

			// Textbox Age
			this->textBoxAge->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->textBoxAge->Size = System::Drawing::Size(64, 30);
			this->textBoxAge->Location = System::Drawing::Point(72, 79);
			this->textBoxAge->Font = (gcnew System::Drawing::Font(L"Calibri", 13, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxAge->Text = "";

			// Textbox DataFilePath
			this->textBoxDataFilePath->Size = System::Drawing::Size(236, 20);
			this->textBoxDataFilePath->Location = System::Drawing::Point(7, 184);
			this->textBoxDataFilePath->Font = (gcnew System::Drawing::Font(L"Times New Roman", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxDataFilePath->Text = "";
			this->textBoxDataFilePath->ReadOnly = true;

			// Date Time Picker
			this->dateTimePicker->Size = System::Drawing::Size(171, 30);
			this->dateTimePicker->Location = System::Drawing::Point(72, 114);
			this->dateTimePicker->Font = (gcnew System::Drawing::Font(L"Calibri", 13, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->dateTimePicker->Format = DateTimePickerFormat::Custom;
			this->dateTimePicker->CustomFormat = "dd/MM/yyyy";

			// Label Title
			this->labelTitle->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelTitle->Size = System::Drawing::Size(150, 30);
			this->labelTitle->Location = System::Drawing::Point((250 - labelTitle->Size.Width) / 2, 7);
			this->labelTitle->Font = (gcnew System::Drawing::Font(L"Calibri", 14, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelTitle->Text = "USER DATA";
			this->labelTitle->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// Label Name
			this->labelName->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelName->Size = System::Drawing::Size(60, 30);
			this->labelName->Location = System::Drawing::Point(7, 44);
			this->labelName->Font = (gcnew System::Drawing::Font(L"Calibri", 13, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelName->Text = "Name";
			this->labelName->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// Label Date
			this->labelDate->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelDate->Size = System::Drawing::Size(60, 30);
			this->labelDate->Location = System::Drawing::Point(7, 114);
			this->labelDate->Font = (gcnew System::Drawing::Font(L"Calibri", 13, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelDate->Text = "Date";
			this->labelDate->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// Check Box Male
			this->checkBoxMale->Size = System::Drawing::Size(70, 30);
			this->checkBoxMale->Location = System::Drawing::Point(150, 79);
			this->checkBoxMale->Font = (gcnew System::Drawing::Font(L"Calibri", 13, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->checkBoxMale->Text = "Male";
			this->checkBoxMale->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// Label Age
			this->labelAge->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelAge->Size = System::Drawing::Size(60, 30);
			this->labelAge->Location = System::Drawing::Point(7, 79);
			this->labelAge->Font = (gcnew System::Drawing::Font(L"Calibri", 13, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelAge->Text = "Age";
			this->labelAge->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// Label DataFilePath
			this->labelDataFilePath->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelDataFilePath->Size = System::Drawing::Size(160, 30);
			this->labelDataFilePath->Location = System::Drawing::Point(7, 149);
			this->labelDataFilePath->Font = (gcnew System::Drawing::Font(L"Calibri", 13, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelDataFilePath->Text = "Data File (if created)";
			this->labelDataFilePath->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->labelDataFilePath->AutoEllipsis = true;
		}
		System::Void buttonBrowse_Click(System::Object^ sender, System::EventArgs^ e)
		{
			openFileDialog1->Filter = "Text files (*.txt)|*.txt|CSV files (*.csv)|*.csv;*.txt|All files (*.*)|*.*";
			openFileDialog1->FilterIndex = 2;
			openFileDialog1->RestoreDirectory = true;
			openFileDialog1->FileName = "";
			if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
			{
				fileName = (char*)Marshal::StringToHGlobalAnsi(openFileDialog1->FileName).ToPointer();
			}
			this->textBoxDataFilePath->Text = gcnew System::String(fileName);
		}
		System::Void buttonConfirm_Click(System::Object^ sender, System::EventArgs^ e) {
			try {
				if (!fileName) {
					if (MessageBox::Show(this, "You have to create new .csv file. Right-click to create and select", "Create data file", System::Windows::Forms::MessageBoxButtons::YesNo) == System::Windows::Forms::DialogResult::Yes) {
						openFileDialog1->Filter = "Text files (*.txt)|*.txt|CSV files (*.csv)|*.csv;*.txt|All files (*.*)|*.*";
						openFileDialog1->FilterIndex = 2;
						openFileDialog1->RestoreDirectory = true;
						openFileDialog1->FileName = "";
						if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
						{
							fileName = (char*)Marshal::StringToHGlobalAnsi(openFileDialog1->FileName).ToPointer();
						}
						userName = (char*)Marshal::StringToHGlobalAnsi(textBoxName->Text).ToPointer();
						userAge = int::Parse(textBoxAge->Text);
						userDate = (char*)Marshal::StringToHGlobalAnsi(dateTimePicker->Text).ToPointer();
						if (checkBoxMale->Checked) userMale = true;
						dataFilePath = (char*)fileName;
						//this->Close();
					}
					else {

					}
				}
				//	}
				else {
					userName = (char*)Marshal::StringToHGlobalAnsi(textBoxName->Text).ToPointer();
					userAge = int::Parse(textBoxAge->Text);
					userDate = (char*)Marshal::StringToHGlobalAnsi(dateTimePicker->Text).ToPointer();
					dataFilePath = (char*)Marshal::StringToHGlobalAnsi(textBoxDataFilePath->Text).ToPointer();
					if (checkBoxMale->Checked) userMale = true;
					//this->Close();
				}
			}
			catch (...) {
				MessageBox::Show(this, "Check your input. Data didn't save.", "Error", System::Windows::Forms::MessageBoxButtons::OK);
				this->Close();
			}
			//string path = dataFilePath;
			//std::replace(path.begin(),path.end(), '\\', '/');
			std::ofstream file;
			if (dataFilePath)
			{
				file.open(dataFilePath, fstream::app);
				if (file.fail()) {
				}
				else {
					struct tm * timeinfo;
					char buffer[80];
					string male;
					time(&rawtime);
					timeinfo = localtime(&rawtime);
					strftime(buffer, 80, "%H:%M:%S,%d/%m/%Y", timeinfo);
					if (userMale) male = "Male"; else male = "Female";
					//file << std::endl << buffer << "," << userDate << std::endl << userName << "," << userAge << "," << male << std::endl;
					file << std::endl << "Beginning time" << "," << "Beginning date" << "," << "Examine day" << endl << buffer << "," << userDate << std::endl << endl << "Name" << "," << "Age" << "," << "Gender" << endl << userName << "," << userAge << "," << male << endl << endl << "Blink count" << "," << "All lid closure count" << "," << "Extended lid closure count" << "," << "Superextended lid closure count" << "," << "State" << "," << "Mean duration of all lid closure" << "," << "Mean duration of all lid closure" << "," << "Mean duration of extended lid closure" << "," << "Mean duration of superextended lid closure" << "," << "Time" << "," << "Contact rate of all lid closure" << "," << "Contact rate of extended lid closure" << "," << "Contact rate of superextended lid closure";
					file.close();
				}
			}
			if (dataFilePath) this->Close();
		}
	};
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
		}

	protected:
		~Form1()
		{
			if (components)
			{
				delete components;
				tempFile.close();
			}
		}
	private:
#pragma region Window_control section
		// Declaration
		System::ComponentModel::IContainer^  components;
		System::Windows::Forms::GroupBox^  groupBoxSource;
		System::Windows::Forms::ComboBox^  comboBoxSource;
		System::Windows::Forms::Button^ buttonSource;
		System::Windows::Forms::OpenFileDialog^  openFileDialog1;
		System::Windows::Forms::PictureBox^ pictureBoxDisp;
		System::Windows::Forms::Button^ buttonStartStop;
		System::Windows::Forms::Button^ buttonReset;
		System::Windows::Forms::GroupBox^ groupBoxToogle;
		System::Windows::Forms::Button^ buttonToogleSleepAlarm;
		System::Windows::Forms::Label^ labelFPSStt;
		System::Windows::Forms::Label^ labelFPS;
		System::Windows::Forms::Label^ labelSleepAlarm;
		System::Windows::Forms::Label^ labelSleepAlarmStt;
		System::Windows::Forms::Label^ labelBlinkCounter;
		System::Windows::Forms::Label^ labelBlinkCounterValue;
		System::Windows::Forms::Timer^  timer1;
		System::Windows::Forms::Label^	labelEyeStt;
		System::Windows::Forms::GroupBox^ groupBoxCounter;
		System::Windows::Forms::GroupBox^ groupBoxAlarm;
		System::Windows::Forms::NumericUpDown^ numericwarningSleepTime;
		System::Windows::Forms::Label^ labelwarningSleepTime;
		System::Windows::Forms::Label^ labelRealTime;
		System::Windows::Forms::Label^ labelWakeAlarm;
		System::Windows::Forms::Button^ buttonToogleWakeAlarm;
		System::Windows::Forms::Label^ labelWakeAlarmStt;
		System::Windows::Forms::Label^ labelwarningWakeTime;
		System::Windows::Forms::NumericUpDown^ numericwarningWakeTime;
		System::Windows::Forms::GroupBox^ groupBoxUser;
		System::Windows::Forms::Button^ buttonUserInfo;
		System::Windows::Forms::Label^ labelUserName;
		System::Windows::Forms::TextBox^ textBoxUserName;
		System::Windows::Forms::Label^ labelFilePath;
		System::Windows::Forms::TextBox^ textBoxFilePath;
		System::Windows::Forms::TrackBar^  trackBar1;
		System::Windows::Forms::Timer^  timer2;
		Capture1::UserForm^ userForm;
		Capture1::DiagnoseForm^ diagnoseForm;
		System::Windows::Forms::Label^ labelLidClosure;
		System::Windows::Forms::Label^ labelLidClosureValue;
		System::Windows::Forms::GroupBox^ groupBoxDetectScale;
		System::Windows::Forms::Label^ labelFaceScale;
		System::Windows::Forms::TextBox^ textBoxFaceScale;
		System::Windows::Forms::Button^ buttonFaceScaleUp;
		System::Windows::Forms::Button^ buttonFaceScaleDown;
		System::Windows::Forms::Label^ labelEyeScale;
		System::Windows::Forms::TextBox^ textBoxEyeScale;
		System::Windows::Forms::Button^ buttonEyeScaleUp;
		System::Windows::Forms::Button^ buttonEyeScaleDown;
		System::Windows::Forms::Button^ buttonRecord;
		System::Windows::Forms::Button^ buttonDiagnose;
		System::Windows::Forms::Button^ buttonAutoDetect;
#pragma endregion
		void InitializeComponent(void)
		{
#pragma region Gcnew section
			this->components = (gcnew System::ComponentModel::Container());
			this->groupBoxCounter = gcnew System::Windows::Forms::GroupBox();
			this->groupBoxAlarm = gcnew System::Windows::Forms::GroupBox();
			this->groupBoxUser = gcnew System::Windows::Forms::GroupBox();
			this->groupBoxSource = (gcnew System::Windows::Forms::GroupBox());
			this->comboBoxSource = (gcnew System::Windows::Forms::ComboBox());
			this->buttonSource = (gcnew System::Windows::Forms::Button());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->pictureBoxDisp = (gcnew System::Windows::Forms::PictureBox());
			this->buttonStartStop = (gcnew System::Windows::Forms::Button());
			this->buttonReset = (gcnew System::Windows::Forms::Button());
			this->groupBoxToogle = (gcnew System::Windows::Forms::GroupBox());
			this->buttonToogleSleepAlarm = (gcnew System::Windows::Forms::Button());
			this->labelFPS = (gcnew System::Windows::Forms::Label());
			this->labelFPSStt = (gcnew System::Windows::Forms::Label());
			this->labelSleepAlarm = (gcnew System::Windows::Forms::Label());
			this->labelSleepAlarmStt = (gcnew System::Windows::Forms::Label());
			this->labelBlinkCounter = (gcnew System::Windows::Forms::Label());
			this->labelBlinkCounterValue = (gcnew System::Windows::Forms::Label());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->timer2 = (gcnew System::Windows::Forms::Timer(this->components));
			this->labelEyeStt = (gcnew System::Windows::Forms::Label());
			this->numericwarningSleepTime = gcnew System::Windows::Forms::NumericUpDown();
			this->labelwarningSleepTime = gcnew System::Windows::Forms::Label();
			this->labelRealTime = gcnew System::Windows::Forms::Label();
			this->labelWakeAlarm = (gcnew System::Windows::Forms::Label());
			this->buttonToogleWakeAlarm = gcnew System::Windows::Forms::Button();
			this->labelWakeAlarmStt = gcnew System::Windows::Forms::Label();
			this->labelwarningWakeTime = gcnew System::Windows::Forms::Label();
			this->numericwarningWakeTime = gcnew System::Windows::Forms::NumericUpDown();
			this->buttonUserInfo = gcnew System::Windows::Forms::Button();
			this->labelUserName = gcnew System::Windows::Forms::Label();
			this->labelFilePath = gcnew System::Windows::Forms::Label();
			this->textBoxUserName = gcnew System::Windows::Forms::TextBox();
			this->textBoxFilePath = gcnew System::Windows::Forms::TextBox();
			this->trackBar1 = (gcnew System::Windows::Forms::TrackBar());
			this->labelLidClosure = gcnew System::Windows::Forms::Label();
			this->labelLidClosureValue = gcnew System::Windows::Forms::Label();
			this->groupBoxDetectScale = gcnew System::Windows::Forms::GroupBox();
			this->labelFaceScale = gcnew System::Windows::Forms::Label();
			this->labelEyeScale = gcnew System::Windows::Forms::Label;
			this->textBoxFaceScale = gcnew System::Windows::Forms::TextBox();
			this->textBoxEyeScale = gcnew System::Windows::Forms::TextBox();
			this->buttonFaceScaleUp = gcnew System::Windows::Forms::Button();
			this->buttonFaceScaleDown = gcnew System::Windows::Forms::Button();
			this->buttonEyeScaleUp = gcnew System::Windows::Forms::Button();
			this->buttonEyeScaleDown = gcnew System::Windows::Forms::Button();
			this->buttonRecord = gcnew System::Windows::Forms::Button();
			this->buttonDiagnose = gcnew System::Windows::Forms::Button();
			this->buttonAutoDetect = gcnew System::Windows::Forms::Button();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxDisp))->BeginInit();
#pragma endregion

			// Set properties for components
#pragma region Component properties section
			// Form 
			this->Controls->Add(this->buttonDiagnose);
			this->Controls->Add(this->buttonRecord);
			this->Controls->Add(this->groupBoxDetectScale);
			this->Controls->Add(this->trackBar1);
			this->Controls->Add(this->groupBoxUser);
			this->Controls->Add(this->groupBoxAlarm);
			this->Controls->Add(this->groupBoxCounter);
			this->Controls->Add(this->groupBoxToogle);
			this->Controls->Add(this->buttonReset);
			this->Controls->Add(pictureBoxDisp);
			this->Controls->Add(this->groupBoxSource);
			this->Controls->Add(this->buttonStartStop);
			this->Controls->Add(this->labelRealTime);
			this->groupBoxSource->Controls->Add(buttonSource);
			this->ClientSize = System::Drawing::Size(900, 680);
			this->Font = (gcnew System::Drawing::Font(L"Calibri", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::Manual;
			this->Text = L"Blinking Detection";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxDisp))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar1))->EndInit();

			// Group Detect Scale
			this->groupBoxDetectScale->Controls->Add(this->buttonEyeScaleDown);
			this->groupBoxDetectScale->Controls->Add(this->buttonEyeScaleUp);
			this->groupBoxDetectScale->Controls->Add(this->buttonFaceScaleDown);
			this->groupBoxDetectScale->Controls->Add(this->buttonFaceScaleUp);
			this->groupBoxDetectScale->Controls->Add(this->textBoxEyeScale);
			this->groupBoxDetectScale->Controls->Add(this->textBoxFaceScale);
			this->groupBoxDetectScale->Controls->Add(this->labelFaceScale);
			this->groupBoxDetectScale->Controls->Add(this->labelEyeScale);
			this->groupBoxDetectScale->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->groupBoxDetectScale->Location = System::Drawing::Point(659, 218);
			this->groupBoxDetectScale->Name = L"groupBoxDetectScale";
			this->groupBoxDetectScale->Size = System::Drawing::Size(225, 130);
			this->groupBoxDetectScale->TabIndex = 10;
			this->groupBoxDetectScale->TabStop = false;
			this->groupBoxDetectScale->Text = L"Detect scale: ";


			// Button AutoDetect
			this->groupBoxDetectScale->Controls->Add(this->buttonAutoDetect);
			this->buttonAutoDetect->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->buttonAutoDetect->Location = System::Drawing::Point(15, 95);
			this->buttonAutoDetect->Name = L"buttonAutoDetect";
			this->buttonAutoDetect->Size = System::Drawing::Size(150, 30);
			this->buttonAutoDetect->TabIndex = 12;
			this->buttonAutoDetect->Text = L"Auto scale off";
			this->buttonAutoDetect->UseVisualStyleBackColor = true;
			this->buttonAutoDetect->Enabled = true;
			this->buttonAutoDetect->Click += gcnew System::EventHandler(this, &Form1::buttonAutoDetect_Click);

			// Button Diagnose
			this->buttonDiagnose->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->buttonDiagnose->Location = System::Drawing::Point(300, 645);
			this->buttonDiagnose->Name = L"buttonDiagnose";
			this->buttonDiagnose->Size = System::Drawing::Size(100, 30);
			this->buttonDiagnose->TabIndex = 12;
			this->buttonDiagnose->Text = L"Diagnose";
			this->buttonDiagnose->UseVisualStyleBackColor = true;
			this->buttonDiagnose->Enabled = true;
			this->buttonDiagnose->Click += gcnew System::EventHandler(this, &Form1::buttonDiagnose_Click);

			// Button Record
			this->buttonRecord->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->buttonRecord->Location = System::Drawing::Point(190, 645);
			this->buttonRecord->ForeColor = System::Drawing::Color::Red;
			this->buttonRecord->Name = L"buttonRecord";
			this->buttonRecord->Size = System::Drawing::Size(100, 30);
			this->buttonRecord->TabIndex = 12;
			this->buttonRecord->Text = L"Record";
			this->buttonRecord->UseVisualStyleBackColor = true;
			this->buttonRecord->Enabled = false;
			this->buttonRecord->Click += gcnew System::EventHandler(this, &Form1::buttonRecord_Click);

			// Label Face Scale
			this->labelFaceScale->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelFaceScale->Font = (gcnew System::Drawing::Font(L"Times New Roman", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelFaceScale->Location = System::Drawing::Point(7, 25);
			this->labelFaceScale->Name = L"labelFaceScale";
			this->labelFaceScale->Size = System::Drawing::Size(50, 30);
			this->labelFaceScale->TabIndex = 2;
			this->labelFaceScale->Text = L"Face";
			this->labelFaceScale->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;

			// Label Face Scale
			this->labelEyeScale->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelEyeScale->Font = (gcnew System::Drawing::Font(L"Times New Roman", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelEyeScale->Location = System::Drawing::Point(7, 60);
			this->labelEyeScale->Name = L"labelEyeScale";
			this->labelEyeScale->Size = System::Drawing::Size(50, 30);
			this->labelEyeScale->TabIndex = 2;
			this->labelEyeScale->Text = L"Eye";
			this->labelEyeScale->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;

			//Textbox Face Scale
			this->textBoxFaceScale->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->textBoxFaceScale->Size = System::Drawing::Size(50, 30);
			this->textBoxFaceScale->Location = System::Drawing::Point(60, 28);
			this->textBoxFaceScale->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxFaceScale->Text = "" + scale_face;
			this->textBoxFaceScale->ReadOnly = true;

			//Textbox Eye Scale
			this->textBoxEyeScale->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->textBoxEyeScale->Size = System::Drawing::Size(50, 30);
			this->textBoxEyeScale->Location = System::Drawing::Point(60, 63);
			this->textBoxEyeScale->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxEyeScale->Text = "" + scale_eye;
			this->textBoxEyeScale->ReadOnly = true;

			// Button Face Scale Up
			this->buttonFaceScaleUp->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->buttonFaceScaleUp->Location = System::Drawing::Point(125, 25);
			this->buttonFaceScaleUp->Name = L"buttonFaceScaleUp";
			this->buttonFaceScaleUp->Size = System::Drawing::Size(30, 30);
			this->buttonFaceScaleUp->TabIndex = 12;
			this->buttonFaceScaleUp->Text = L"U";
			this->buttonFaceScaleUp->UseVisualStyleBackColor = true;
			this->buttonFaceScaleUp->Click += gcnew System::EventHandler(this, &Form1::buttonFaceScaleUp_Click);

			// Button Face Scale Down
			this->buttonFaceScaleDown->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->buttonFaceScaleDown->Location = System::Drawing::Point(165, 25);
			this->buttonFaceScaleDown->Name = L"buttonFaceScaleDown";
			this->buttonFaceScaleDown->Size = System::Drawing::Size(30, 30);
			this->buttonFaceScaleDown->TabIndex = 12;
			this->buttonFaceScaleDown->Text = L"D";
			this->buttonFaceScaleDown->UseVisualStyleBackColor = true;
			this->buttonFaceScaleDown->Click += gcnew System::EventHandler(this, &Form1::buttonFaceScaleDown_Click);

			// Button Eye Scale Up
			this->buttonEyeScaleUp->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->buttonEyeScaleUp->Location = System::Drawing::Point(125, 60);
			this->buttonEyeScaleUp->Name = L"buttonEyeScaleUp";
			this->buttonEyeScaleUp->Size = System::Drawing::Size(30, 30);
			this->buttonEyeScaleUp->TabIndex = 12;
			this->buttonEyeScaleUp->Text = L"U";
			this->buttonEyeScaleUp->UseVisualStyleBackColor = true;
			this->buttonEyeScaleUp->Click += gcnew System::EventHandler(this, &Form1::buttonEyeScaleUp_Click);

			// Button Face Scale Up
			this->buttonEyeScaleDown->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->buttonEyeScaleDown->Location = System::Drawing::Point(165, 60);
			this->buttonEyeScaleDown->Name = L"buttonEyeScaleDown";
			this->buttonEyeScaleDown->Size = System::Drawing::Size(30, 30);
			this->buttonEyeScaleDown->TabIndex = 12;
			this->buttonEyeScaleDown->Text = L"D";
			this->buttonEyeScaleDown->UseVisualStyleBackColor = true;
			this->buttonEyeScaleDown->Click += gcnew System::EventHandler(this, &Form1::buttonEyeScaleDown_Click);

			// Group Box User
			this->groupBoxUser->Controls->Add(this->textBoxFilePath);
			this->groupBoxUser->Controls->Add(this->labelFilePath);
			this->groupBoxUser->Controls->Add(this->buttonUserInfo);
			this->groupBoxUser->Controls->Add(this->comboBoxSource);
			this->groupBoxUser->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->groupBoxUser->Location = System::Drawing::Point(20, 580);
			this->groupBoxUser->Name = L"groupBoxUser";
			this->groupBoxUser->Size = System::Drawing::Size(382, 62);
			this->groupBoxUser->TabIndex = 10;
			this->groupBoxUser->TabStop = false;
			this->groupBoxUser->Text = L"User information";

			//trackbar1
			this->trackBar1->AutoSize = false;
			this->trackBar1->Location = System::Drawing::Point(13, 550);
			this->trackBar1->Name = L"trackBar1";
			this->trackBar1->Size = System::Drawing::Size(630, 26);
			this->trackBar1->TabIndex = 1;
			this->trackBar1->Scroll += gcnew System::EventHandler(this, &Form1::trackBar1_Scroll);
			this->trackBar1->Enabled = false;
			// Button User Info

			this->buttonUserInfo->Location = System::Drawing::Point(7, 23);
			this->buttonUserInfo->Name = L"buttonUserInfo";
			this->buttonUserInfo->Size = System::Drawing::Size(79, 30);
			this->buttonUserInfo->TabIndex = 1;
			this->buttonUserInfo->Text = L"Add";
			this->buttonUserInfo->UseVisualStyleBackColor = true;
			this->buttonUserInfo->Click += gcnew System::EventHandler(this, &Form1::buttonUserInfo_Click);


			// Label FilePath
			this->labelFilePath->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelFilePath->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelFilePath->Location = System::Drawing::Point(91, 23);
			this->labelFilePath->Name = L"labelFilePath";
			this->labelFilePath->Size = System::Drawing::Size(100, 30);
			this->labelFilePath->TabIndex = 1;
			this->labelFilePath->Text = L"Data file path";
			this->labelFilePath->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;

			// Textbox FilePath
			this->textBoxFilePath->Size = System::Drawing::Size(180, 30);
			this->textBoxFilePath->Location = System::Drawing::Point(196, 26);
			this->textBoxFilePath->Font = (gcnew System::Drawing::Font(L"Times New Roman", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->textBoxFilePath->Text = "";
			this->textBoxFilePath->ReadOnly = true;

			// Label Real Time
			this->labelRealTime->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelRealTime->Location = System::Drawing::Point(20, 645);
			this->labelRealTime->Size = System::Drawing::Size(230, 30);
			this->labelRealTime->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// Timer
			this->timer1->Interval = 30;
			this->timer1->Tick += gcnew System::EventHandler(this, &Form1::timer1_Tick);
			// Timer
			this->timer2->Interval = 30;
			this->timer2->Tick += gcnew System::EventHandler(this, &Form1::timer2_Tick);

			// Group Box Counter
			this->groupBoxCounter->Controls->Add(this->labelEyeStt);
			this->groupBoxCounter->Controls->Add(this->labelLidClosure);
			this->groupBoxCounter->Controls->Add(this->labelLidClosureValue);
			this->groupBoxCounter->Controls->Add(this->labelBlinkCounterValue);
			this->groupBoxCounter->Controls->Add(this->labelBlinkCounter);
			this->groupBoxCounter->Controls->Add(this->comboBoxSource);
			this->groupBoxCounter->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->groupBoxCounter->Location = System::Drawing::Point(659, 347);
			this->groupBoxCounter->Name = L"groupBoxCounter";
			this->groupBoxCounter->Size = System::Drawing::Size(225, 260);
			this->groupBoxCounter->TabIndex = 10;
			this->groupBoxCounter->TabStop = false;
			this->groupBoxCounter->Text = L"Counter";

			// Label Lid Closure
			this->labelLidClosure->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelLidClosure->Font = (gcnew System::Drawing::Font(L"Times New Roman", 14, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelLidClosure->Location = System::Drawing::Point(7, 200);
			this->labelLidClosure->Name = L"labelLidClosure";
			this->labelLidClosure->Size = System::Drawing::Size(211, 30);
			this->labelLidClosure->TabIndex = 2;
			this->labelLidClosure->Text = L"Last closure duration (ms)";
			this->labelLidClosure->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;

			// Label Lid Closure Value
			this->labelLidClosureValue->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->labelLidClosureValue->Font = (gcnew System::Drawing::Font(L"Times New Roman", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelLidClosureValue->Location = System::Drawing::Point(7, 228);
			this->labelLidClosureValue->Name = L"labelLidClosureValue";
			this->labelLidClosureValue->Size = System::Drawing::Size(211, 30);
			this->labelLidClosureValue->TabIndex = 2;
			this->labelLidClosureValue->Text = L"";
			this->labelLidClosureValue->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;

			// Group Box Alarm
			this->groupBoxAlarm->Controls->Add(this->labelWakeAlarm);
			this->groupBoxAlarm->Controls->Add(this->labelSleepAlarm);
			this->groupBoxAlarm->Controls->Add(this->comboBoxSource);
			this->groupBoxAlarm->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->groupBoxAlarm->Location = System::Drawing::Point(337, 3);
			this->groupBoxAlarm->Name = L"groupBoxAlarm";
			this->groupBoxAlarm->Size = System::Drawing::Size(315, 62);
			this->groupBoxAlarm->TabIndex = 10;
			this->groupBoxAlarm->TabStop = false;
			this->groupBoxAlarm->Text = L"Alarm";

			// Group Box Source
			this->groupBoxSource->Controls->Add(this->comboBoxSource);
			this->groupBoxSource->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->groupBoxSource->Location = System::Drawing::Point(5, 3);
			this->groupBoxSource->Name = L"groupBoxSource";
			this->groupBoxSource->Size = System::Drawing::Size(333, 62);
			this->groupBoxSource->TabIndex = 10;
			this->groupBoxSource->TabStop = false;
			this->groupBoxSource->Text = L"Source";

			// Combo Box Source

			this->comboBoxSource->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBoxSource->FormattingEnabled = true;
			this->comboBoxSource->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"Capture From Camera", L"Capture From File" });
			this->comboBoxSource->Location = System::Drawing::Point(8, 23);
			this->comboBoxSource->Name = L"comboBoxSource";
			this->comboBoxSource->Size = System::Drawing::Size(235, 27);
			this->comboBoxSource->TabIndex = 0;
			this->comboBoxSource->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBoxSource_SelectedIndexChanged);

			// Button Source

			this->buttonSource->Location = System::Drawing::Point(249, 22);
			this->buttonSource->Name = L"buttonSource";
			this->buttonSource->Size = System::Drawing::Size(79, 30);
			this->buttonSource->TabIndex = 1;
			this->buttonSource->Text = L"Start";
			this->buttonSource->UseVisualStyleBackColor = true;
			this->buttonSource->Click += gcnew System::EventHandler(this, &Form1::buttonSource_Click);

			// Picture Box Disp

			this->pictureBoxDisp->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBoxDisp->Location = System::Drawing::Point(10, 70);
			this->pictureBoxDisp->Name = L"pictureBoxDisp";
			this->pictureBoxDisp->Size = System::Drawing::Size(640, 480);
			this->pictureBoxDisp->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBoxDisp->TabIndex = 0;
			this->pictureBoxDisp->TabStop = false;
			this->pictureBoxDisp->BackColor = System::Drawing::Color::LightSlateGray;
			this->pictureBoxDisp->Image = System::Drawing::Bitmap::FromFile("blank.png");
			this->pictureBoxDisp->Click += gcnew System::EventHandler(this, &Form1::pictureBoxDisp_Click_1);

			// Button Start Stop

			this->buttonStartStop->Font = (gcnew System::Drawing::Font(L"Calibri", 14.25F));
			this->buttonStartStop->Location = System::Drawing::Point(410, 590);
			this->buttonStartStop->Name = L"buttonStartStop";
			this->buttonStartStop->Size = System::Drawing::Size(225, 30);
			this->buttonStartStop->TabIndex = 2;
			this->buttonStartStop->Text = L"Start counting";
			this->buttonStartStop->UseVisualStyleBackColor = true;
			this->buttonStartStop->Click += gcnew System::EventHandler(this, &Form1::buttonStartStop_Click);

			// Button Reset

			this->buttonReset->Font = (gcnew System::Drawing::Font(L"Calibri", 14.25F));
			this->buttonReset->Location = System::Drawing::Point(410, 625);
			this->buttonReset->Name = L"buttonReset";
			this->buttonReset->Size = System::Drawing::Size(225, 30);
			this->buttonReset->TabIndex = 12;
			this->buttonReset->Text = L"Reset counter";
			this->buttonReset->UseVisualStyleBackColor = true;
			this->buttonReset->Click += gcnew System::EventHandler(this, &Form1::buttonReset_Click);

			// Group Box Toogle
			this->groupBoxToogle->Controls->Add(this->labelWakeAlarmStt);
			this->groupBoxToogle->Controls->Add(this->buttonToogleWakeAlarm);
			this->groupBoxToogle->Controls->Add(this->labelwarningWakeTime);
			this->groupBoxToogle->Controls->Add(this->labelwarningSleepTime);
			this->groupBoxToogle->Controls->Add(this->numericwarningWakeTime);
			this->groupBoxToogle->Controls->Add(this->numericwarningSleepTime);
			this->groupBoxToogle->Controls->Add(this->labelFPS);
			this->groupBoxToogle->Controls->Add(this->labelFPSStt);
			this->groupBoxToogle->Controls->Add(this->labelSleepAlarmStt);
			this->groupBoxToogle->Controls->Add(this->buttonToogleSleepAlarm);
			this->groupBoxToogle->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->groupBoxToogle->Location = System::Drawing::Point(659, 5);
			this->groupBoxToogle->Name = L"groupBoxToogle";
			this->groupBoxToogle->Size = System::Drawing::Size(225, 211);
			this->groupBoxToogle->TabIndex = 11;
			this->groupBoxToogle->TabStop = false;
			this->groupBoxToogle->Text = L"Options:";
			this->groupBoxToogle->Enter += gcnew System::EventHandler(this, &Form1::groupBoxToogle_Enter);

			// Label Warning Time Sleep
			//this->labelwarningSleepTime->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->labelwarningSleepTime->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelwarningSleepTime->Location = System::Drawing::Point(30, 95);
			this->labelwarningSleepTime->Name = L"labelwarningSleepTime";
			this->labelwarningSleepTime->Size = System::Drawing::Size(105, 30);
			this->labelwarningSleepTime->TabIndex = 2;
			this->labelwarningSleepTime->Text = L"Asleep after:  ";
			this->labelwarningSleepTime->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;

			// Label Warning Time Wake
			//this->labelwarningWakeTime->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->labelwarningWakeTime->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelwarningWakeTime->Location = System::Drawing::Point(30, 168);
			this->labelwarningWakeTime->Name = L"labelwarningWakeTime";
			this->labelwarningWakeTime->Size = System::Drawing::Size(105, 30);
			this->labelwarningWakeTime->TabIndex = 2;
			this->labelwarningWakeTime->Text = L"Awake after: ";
			this->labelwarningWakeTime->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;

			// Numeric Warning Time
			this->numericwarningSleepTime->Location = System::Drawing::Point(138, 98);
			this->numericwarningSleepTime->Size = System::Drawing::Size(50, 30);
			this->numericwarningSleepTime->Minimum = 3;
			this->numericwarningSleepTime->Maximum = 10;
			this->numericwarningSleepTime->Value = warningSleepTime;
			this->numericwarningSleepTime->ValueChanged += gcnew System::EventHandler(this, &Form1::numericwarningSleepTime_ValueChanged);

			// Numeric Warning Time
			this->numericwarningWakeTime->Location = System::Drawing::Point(138, 171);
			this->numericwarningWakeTime->Size = System::Drawing::Size(50, 30);
			this->numericwarningWakeTime->Minimum = 0;
			this->numericwarningWakeTime->Maximum = 10;
			this->numericwarningWakeTime->Value = warningWakeTime;
			this->numericwarningWakeTime->ValueChanged += gcnew System::EventHandler(this, &Form1::numericwarningWakeTime_ValueChanged);

			// Button Sleep Alarm
			this->buttonToogleSleepAlarm->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->buttonToogleSleepAlarm->Location = System::Drawing::Point(7, 63);
			this->buttonToogleSleepAlarm->Name = L"buttonToogleSleepAlarm";
			this->buttonToogleSleepAlarm->Size = System::Drawing::Size(150, 30);
			this->buttonToogleSleepAlarm->TabIndex = 12;
			this->buttonToogleSleepAlarm->Text = L"Sleep Alarm Voice";
			this->buttonToogleSleepAlarm->UseVisualStyleBackColor = true;
			this->buttonToogleSleepAlarm->Click += gcnew System::EventHandler(this, &Form1::buttonToogleSleepAlarm_Click);

			// Button Wake Alarm
			this->buttonToogleWakeAlarm->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->buttonToogleWakeAlarm->Location = System::Drawing::Point(7, 136);
			this->buttonToogleWakeAlarm->Name = L"buttonToogleWakeAlarm";
			this->buttonToogleWakeAlarm->Size = System::Drawing::Size(150, 30);
			this->buttonToogleWakeAlarm->TabIndex = 12;
			this->buttonToogleWakeAlarm->Text = L"Wake Alarm Voice";
			this->buttonToogleWakeAlarm->UseVisualStyleBackColor = true;
			this->buttonToogleWakeAlarm->Click += gcnew System::EventHandler(this, &Form1::buttonToogleWakeAlarm_Click);

			// Label Wake Alarm stt
			this->labelWakeAlarmStt->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelWakeAlarmStt->Location = System::Drawing::Point(165, 136);
			this->labelWakeAlarmStt->Name = L"labelWakeAlarmStt";
			this->labelWakeAlarmStt->Size = System::Drawing::Size(40, 25);
			this->labelWakeAlarmStt->TabIndex = 2;
			this->labelWakeAlarmStt->Text = L"OFF";
			this->labelWakeAlarmStt->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;

			// Label fps stt

			this->labelFPSStt->Font = (gcnew System::Drawing::Font(L"Calibri", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelFPSStt->Location = System::Drawing::Point(10, 29);
			this->labelFPSStt->Name = L"labelFPSStt";
			this->labelFPSStt->Size = System::Drawing::Size(50, 25);
			this->labelFPSStt->TabIndex = 2;
			this->labelFPSStt->Text = L"FPS";
			this->labelFPSStt->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;

			// Label Sleep Alarm stt

			this->labelSleepAlarmStt->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelSleepAlarmStt->Location = System::Drawing::Point(165, 65);
			this->labelSleepAlarmStt->Name = L"labelSleepAlarmStt";
			this->labelSleepAlarmStt->Size = System::Drawing::Size(40, 25);
			this->labelSleepAlarmStt->TabIndex = 2;
			this->labelSleepAlarmStt->Text = L"OFF";
			this->labelSleepAlarmStt->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;

			// Label fps
			this->labelFPS->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->labelFPS->Font = (gcnew System::Drawing::Font(L"Calibri", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelFPS->Location = System::Drawing::Point(60, 29);
			this->labelFPS->Name = L"labelFPS";
			this->labelFPS->Size = System::Drawing::Size(50, 25);
			this->labelFPS->TabIndex = 2;
			this->labelFPS->Text = L"";
			this->labelFPS->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;

			// Label Sleep Alarm
			this->labelSleepAlarm->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->labelSleepAlarm->ForeColor = System::Drawing::Color::Gray;
			this->labelSleepAlarm->Font = (gcnew System::Drawing::Font(L"Calibri", 24, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelSleepAlarm->Location = System::Drawing::Point(3, 18);
			this->labelSleepAlarm->Name = L"labelSleepAlarm";
			this->labelSleepAlarm->Size = System::Drawing::Size(150, 40);
			this->labelSleepAlarm->TabIndex = 2;
			this->labelSleepAlarm->Text = L"ASLEEP";
			this->labelSleepAlarm->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// Label Wake Alarm
			this->labelWakeAlarm->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->labelWakeAlarm->ForeColor = System::Drawing::Color::Gray;
			this->labelWakeAlarm->Font = (gcnew System::Drawing::Font(L"Calibri", 24, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelWakeAlarm->Location = System::Drawing::Point(161, 18);
			this->labelWakeAlarm->Name = L"labelWakeAlarm";
			this->labelWakeAlarm->Size = System::Drawing::Size(150, 40);
			this->labelWakeAlarm->TabIndex = 2;
			this->labelWakeAlarm->Text = L"AWAKE";
			this->labelWakeAlarm->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// Label blink counter

			this->labelBlinkCounter->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->labelBlinkCounter->Font = (gcnew System::Drawing::Font(L"Times New Roman", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelBlinkCounter->Location = System::Drawing::Point(7, 27);
			this->labelBlinkCounter->Name = L"labelBlinkCounter";
			this->labelBlinkCounter->Size = System::Drawing::Size(211, 35);
			this->labelBlinkCounter->TabIndex = 2;
			this->labelBlinkCounter->Text = L"BLINK COUNTER";
			this->labelBlinkCounter->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;

			// Label blink counter value

			this->labelBlinkCounterValue->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->labelBlinkCounterValue->Font = (gcnew System::Drawing::Font(L"Times New Roman", 30, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelBlinkCounterValue->Location = System::Drawing::Point(37, 70);
			this->labelBlinkCounterValue->Name = L"labelBlinkCounterValue";
			this->labelBlinkCounterValue->Size = System::Drawing::Size(151, 60);
			this->labelBlinkCounterValue->TabIndex = 2;
			this->labelBlinkCounterValue->Text = L"0";
			this->labelBlinkCounterValue->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;

			// Label eye stt
			this->labelEyeStt->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->labelEyeStt->Font = (gcnew System::Drawing::Font(L"Times New Roman", 24, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelEyeStt->Location = System::Drawing::Point(7, 138);
			this->labelEyeStt->Name = L"labelEyeSttValue";
			this->labelEyeStt->Size = System::Drawing::Size(211, 50);
			this->labelEyeStt->TabIndex = 2;
			this->labelEyeStt->Text = L"";
			this->labelEyeStt->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
		}
#pragma endregion

		System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
			try {
				if (m_faceDetector == NULL) {
					m_faceDetector = new cv::CascadeClassifier(faceCascadePath);
				}
				else {
					m_faceDetector->load(faceCascadePath);
				}
			}
			catch (cv::Exception e) {}
			if (m_faceDetector->empty()) {
				fprintf(stderr, "ERROR: Could not load classifier cascade\n");
				return;
			}
			// Load eye detector
			try {
				if (m_eyeDetector == NULL) {
					m_eyeDetector = new cv::CascadeClassifier(eyeCascadePath);
				}
				else {
					m_eyeDetector->load(eyeCascadePath);
				}
			}
			catch (cv::Exception e) {}
			if (m_eyeDetector->empty()) {
				fprintf(stderr, "ERROR: Could not load classifier cascade\n");
				return;
			}
			SleepAlarm = false;
			counting = false;
			timer2->Start();
		}
		System::Void buttonAutoDetect_Click(System::Object^  sender, System::EventArgs^  e) {
			if (!autoMode) {
				autoMode = true;
				firstDetect = true;
				this->buttonAutoDetect->Text = "Auto scale on";
				saveFS = scale_face;
				saveES = scale_eye;
				this->buttonFaceScaleUp->Enabled = false;
				this->buttonFaceScaleDown->Enabled = false;
				this->buttonEyeScaleUp->Enabled = false;
				this->buttonEyeScaleDown->Enabled = false;
			}
			else if (autoMode) {
				autoMode = false;
				firstDetect = false;
				this->buttonAutoDetect->Text = "Auto scale off";
				scale_face = saveFS;
				scale_eye = saveES;
				this->textBoxFaceScale->Text = "" + scale_face;
				this->textBoxEyeScale->Text = "" + scale_eye;
				this->buttonFaceScaleUp->Enabled = true;
				this->buttonFaceScaleDown->Enabled = true;
				this->buttonEyeScaleUp->Enabled = true;
				this->buttonEyeScaleDown->Enabled = true;
			}
		}
		System::Void buttonRecord_Click(System::Object^  sender, System::EventArgs^  e) {
			if (!record) {
				record = true;
				recordStart = clock();
				this->buttonRecord->Text = "Stop";
			}
			else if (record) {
				record = false;
				tempFile << endl << ",,,," << "Summary";
				tempFile << std::endl << "Blink count" << "," << "All lid closure count" << "," << "Extended lid closure count" << "," << "Superextended lid closure count" << "," << "Mean duration of all lid closure" << "," << "Mean duration of extended lid closure" << "," << "Mean duration of superextended lid closure" << "," << "Contact rate of all lid closure" << "," << "Contact rate of extended lid closure" << "," << "Contact rate of superextended lid closure";
				tempFile << endl << blinkCount << "," << allLidClosure << "," << extendLidClosure << "," << superExtendLidClosure;
				tempFile << "," << a << "," << ex << "," << se << ",";
				tempFile << avgA << "," << avgE << "," << avgSE << std::endl;
				tempFile << endl << "Result,";
				int dryeyes = 0;
				double ratioCPMA = avgA / avgAnorm;
				double ratioCPME = avgE / avgEnorm;
				double ratioCPMSE = avgSE / avgSEnorm;
				double ratioACTA = a / aNorm;
				double ratioACTE = ex / eNorm;
				double ratioACTSE = se / seNorm;
				if (ratioCPMA > 2.47 && ratioCPMA < 16.46) dryeyes++;
				if (ratioCPME > 3.35 && ratioCPME < 47.26) dryeyes++;
				if (ratioCPMSE > 17.73 && ratioCPMSE < 113.16) dryeyes++;
				if (ratioACTA > 2.46 && ratioACTA < 20.33) dryeyes++;
				if (ratioACTE > 1.65 && ratioACTE < 11.07) dryeyes++;
				if (ratioACTSE > 3.28 && ratioACTSE < 6.15) dryeyes++;
				if (dryeyes < 2) tempFile << "Non-dry eyes";
				else if (dryeyes < 5) tempFile << "Dry eyes potential";
				else if (dryeyes <= 6) tempFile << "Dry eyes";
				avgE = 0;
				avgA = 0;
				avgSE = 0;
				a = 0; ex = 0; se = 0;
				this->buttonRecord->Text = "Record";
			}
		}
		System::Void buttonFaceScaleUp_Click(System::Object^  sender, System::EventArgs^  e) {
			if (scale_face < 2) {
				scale_face = scale_face + 0.05;
				this->textBoxFaceScale->Text = "" + scale_face;
			}
		}
		System::Void buttonFaceScaleDown_Click(System::Object^  sender, System::EventArgs^  e) {
			if (scale_face > 0.15) {
				scale_face = scale_face - 0.05;
				this->textBoxFaceScale->Text = "" + scale_face;
			}
		}
		System::Void buttonEyeScaleUp_Click(System::Object^  sender, System::EventArgs^  e) {
			if (scale_eye < 3) {
				scale_eye = scale_eye + 0.05;
				this->textBoxEyeScale->Text = "" + scale_eye;
			}
		}
		System::Void buttonEyeScaleDown_Click(System::Object^  sender, System::EventArgs^  e) {
			if (scale_eye > 0.2) {
				scale_eye = scale_eye - 0.05;
				this->textBoxEyeScale->Text = "" + scale_eye;
			}
		}
		System::Void comboBoxSource_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		}
		System::Void buttonSource_Click(System::Object^  sender, System::EventArgs^  e) {
			if (comboBoxSource->Text == "")
			{
				MessageBox::Show(this, "Select Capture Method", "Error");
			}
			if (buttonSource->Text == "Start")
			{
				if (comboBoxSource->Text == "Capture From Camera")
				{
					capture.release();
					capture = cv::VideoCapture(0);
					buttonSource->Text = "Stop";
					m_capture = &capture;
					trackBar1->Minimum = 0;
					trackBar1->Maximum = 0;
					tick = 0;
					frameCounter = 0;
					start = time(0);
					timer1->Start();
				}
				else if (comboBoxSource->Text == "Capture From File")
				{
					char *fileName;
					openFileDialog1->Filter = "MP4 files (*.mp4)|*.mp4|AVI files (*.avi)|*.avi|All files (*.*)|*.*";
					openFileDialog1->FilterIndex = 2;
					openFileDialog1->RestoreDirectory = true;
					openFileDialog1->FileName = "";
					if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
					{
						fileName = (char*)Marshal::StringToHGlobalAnsi(openFileDialog1->FileName).ToPointer();
					}
					if (fileName) {
						this->trackBar1->Enabled = true;
						capture.release();
						capture = cv::VideoCapture(fileName);
						trackBar1->Minimum = 0;
						trackBar1->Maximum = (int)capture.get(CV_CAP_PROP_FRAME_COUNT);
						buttonSource->Text = "Stop";
						m_capture = &capture;
						tick = 0;
						frameCounter = 0;
						start = time(0);
						timer1->Start();
					}
				}
			}
			else if (buttonSource->Text == "Stop")
			{
				buttonSource->Text = "Start";
				capture.release();
				timer1->Stop();
				this->trackBar1->Enabled = false;
				this->pictureBoxDisp->Image = System::Drawing::Bitmap::FromFile("blank.png");
			}
		}
		System::Void pictureBoxDisp_Click_1(System::Object^  sender, System::EventArgs^  e) {
		}
		System::Void buttonStartStop_Click(System::Object^  sender, System::EventArgs^  e) {
			if (!counting) {
				buttonStartStop->Text = "Stop counting";
				counting = true;
			}
			else if (counting) {
				buttonStartStop->Text = "Start counting";
				counting = false;
			}
		}
		System::Void buttonReset_Click(System::Object^  sender, System::EventArgs^  e) {
			blinkCount = 0;
			labelBlinkCounterValue->Text = "" + blinkCount;
		}
		System::Void groupBoxToogle_Enter(System::Object^  sender, System::EventArgs^  e) {
		}
		System::Void buttonToogleSleepAlarm_Click(System::Object^  sender, System::EventArgs^  e) {
			if (!SleepAlarm) {
				labelSleepAlarmStt->Text = "ON";
				SleepAlarm = true;
			}
			else if (SleepAlarm)
			{
				labelSleepAlarmStt->Text = "OFF";
				SleepAlarm = false;
				this->labelSleepAlarm->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
				this->labelSleepAlarm->ForeColor = System::Drawing::Color::Gray;
			}
		}
		System::Void buttonToogleWakeAlarm_Click(System::Object^  sender, System::EventArgs^  e) {
			if (!WakeAlarm) {
				labelWakeAlarmStt->Text = "ON";
				WakeAlarm = true;
			}
			else if (WakeAlarm)
			{
				labelWakeAlarmStt->Text = "OFF";
				WakeAlarm = false;
				this->labelWakeAlarm->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
				this->labelWakeAlarm->ForeColor = System::Drawing::Color::Gray;
			}
		}
		System::Void trackBar1_Scroll(System::Object^  sender, System::EventArgs^  e)
		{
			capture.set(CV_CAP_PROP_POS_FRAMES, trackBar1->Value);
		}
		System::Void timer2_Tick(System::Object^  sender, System::EventArgs^  e)
		{
			/* Time */
			struct tm * timeinfo;
			char buffer[80];
			time(&rawtime);
			timeinfo = localtime(&rawtime);
			strftime(buffer, 80, "Real-time: %H:%M:%S", timeinfo);
			this->labelRealTime->Text = gcnew System::String(buffer);
		}
		System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e)
		{
			if (!fileOpened) {
				if (dataFilePath) {
					tempFile.open(dataFilePath, fstream::app);
					if (tempFile.fail()) {
						MessageBox::Show(this, "File open fail. I'm so noob that i can't handle with file path in vietnamese. Data cannot be writed.", "Error", System::Windows::Forms::MessageBoxButtons::OK);
						dataFilePath = NULL;
					}
					else {
						this->buttonUserInfo->Text = "Remove";
						this->buttonRecord->Enabled = true;
						this->textBoxFilePath->Text = gcnew System::String(dataFilePath);
						fileOpened = true;
					}
				}
				else {
					this->buttonUserInfo->Text = "Add";
					this->groupBoxUser->Text = "User information - Add to record data";
				}
			}
			try
			{
				captureFrame();
				if (!frame.empty()) {
					if (autoMode) {
						if (firstDetect) {
							scale_face = saveFS;
							scale_eye = saveES;
							firstDetect = false;
						}
						else {
							if (FRW != 0) {
								scale_face = (double) fixedFaceH / FRW;
								scale_eye = (double) fixedEyeH / (FRW * 0.3);
							}
							else {
								scale_face = saveFS;
								scale_eye = saveES;
							}
						}
						this->textBoxFaceScale->Text = "" + scale_face;
						this->textBoxEyeScale->Text = "" + scale_eye;
					}
					detectFace();
					if (faceFound) {
						if (!facePrevStt) beginSleep = tick;
						detectEye();
						if (eyeFound) {												// Display eye stt
							labelEyeStt->Text = "EYE OPEN";
							labelEyeStt->ForeColor = System::Drawing::Color::Blue;
						}
						if (!eyeFound) {											// Display eye stt
							labelEyeStt->Text = "EYE CLOSE";
							labelEyeStt->ForeColor = System::Drawing::Color::Red;
						}
						// Closure Time Calculate
						if (eyePrevStt && !eyeFound) {
							lidClose = clock();
						}
						if (!eyePrevStt && eyeFound) {
							lidOpen = clock();
							closureTime = diffclock(lidOpen, lidClose);
							this->labelLidClosureValue->Text = L"" + closureTime;
							if (counting) blinkCount++;
							if (record) {
								clock_t cal = clock();
								double durCal = diffclock(cal, recordStart);
								if (closureTime > 500) {
									superExtendLidClosure++;
									superExtendClosureTotalTime = superExtendClosureTotalTime + closureTime;
								}
								if (closureTime > 100) {
									extendLidClosure++;
									extendLidClosureTotalTime = extendLidClosureTotalTime + closureTime;
								}
								allLidClosure++;
								allLidClosureTotalTime = allLidClosureTotalTime + closureTime;
								avgE = extendLidClosureTotalTime * 100 / durCal;
								avgSE = superExtendClosureTotalTime * 100 / durCal;
								avgA = allLidClosureTotalTime * 100 / durCal;
								if (superExtendLidClosure != 0) se = superExtendClosureTotalTime / superExtendLidClosure / 1000; else se = 0;
								if (extendLidClosure != 0) ex = extendLidClosureTotalTime / extendLidClosure / 1000; else ex = 0;
								if (allLidClosure != 0) a = allLidClosureTotalTime / allLidClosure / 1000; else a = 0;
								//Print to .csv
								string state;
								if (sleepState) state = "sleep";
								else if (wakeState) state = "wake";
								else state = "undefined";
								tempFile << endl << blinkCount << "," << allLidClosure << "," << extendLidClosure << "," << superExtendLidClosure << ",";
								tempFile << state << "," << a << "," << ex << "," << se << ",";
								tempFile << durCal << "," << avgA << "," << avgE << "," << avgSE << std::endl;
							}
						}
						// Record Data
						//tempFile << tick << ",";
						//if (eyePrevStt && !eyeFound) {
						//	tempFile << "blink" << ",";
						//}
						//else {
						//	tempFile << ",";
						//}
						eyePrevStt = eyeFound;
						// Define sleep duration
						if (eyePrevStt) {
							beginSleep = tick;
						}
						else {
							endSleep = tick;
						}
						// Define sleep state
						if (endSleep - beginSleep >= warningSleepTime && tick >= sleepWarningEnd) {
							sleepWarningEnd = tick + sleepAlarmVoiceDuration;
							sleepState = true;
							wakeState = false;
							if (SleepAlarm) PlaySound(TEXT("SleepAlarm.wav"), NULL, 0x20000 | 1 | 1);
						}
						else if (endSleep - beginSleep >= warningSleepTime && tick < sleepWarningEnd) {
							sleepState = true;
							wakeState = false;
						}
						else
						{
							if (sleepState) {
								beginWake = tick + warningWakeTime;
								ringWakeAlarm = true;
							}
							sleepState = false;
						}
						// Display wake state
						if (tick >= beginWake && !sleepState) {
							wakeState = true;
						}
						// Display sleep state
						if (sleepState) {
							this->labelSleepAlarm->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
							this->labelSleepAlarm->ForeColor = System::Drawing::Color::Red;
						}
						else {
							this->labelSleepAlarm->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
							this->labelSleepAlarm->ForeColor = System::Drawing::Color::Gray;
						}
						// Display Wake state
						if (wakeState) {
							this->labelWakeAlarm->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
							this->labelWakeAlarm->ForeColor = System::Drawing::Color::Green;
							if (WakeAlarm && ringWakeAlarm) {
								PlaySound(TEXT("WakeAlarm.wav"), NULL, 0x20000 | 1 | 1);
								ringWakeAlarm = false;
							}
						}
						else {
							this->labelWakeAlarm->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
							this->labelWakeAlarm->ForeColor = System::Drawing::Color::Gray;
						}
						// End sleep alarm
					}
					facePrevStt = faceFound;
					drawn.copyTo(result);
					//char buffer[80];
					//if (counting) {
					//	tempFile << buffer << "," << blinkCount << "," << sleepState << "," << wakeState << std::endl;
					//}
					//else {
					//	tempFile << buffer << "," << "OFF" << "," << sleepState << "," << wakeState << std::endl;
					//}
					labelFPS->Text = "" + (int)fps;
					if (counting) {
						labelBlinkCounterValue->Text = "" + (int)blinkCount;
					}
					pictureBoxDisp->Image = gcnew System::Drawing::Bitmap(drawn.cols, drawn.rows, drawn.step, System::Drawing::Imaging::PixelFormat::Format24bppRgb, (System::IntPtr) drawn.data);
					pictureBoxDisp->Refresh();
					resetVar();
					frameCounter++;
					dur = time(0) - start;
					if (dur - tick >= 1)
					{
						tick++;
						fps = frameCounter;
						frameCounter = 0;
					}
					trackBar1->Value = (int)capture.get(CV_CAP_PROP_POS_FRAMES);
				}
			}
			catch (...) {}
		}
		System::Void numericwarningSleepTime_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
			warningSleepTime = (int)this->numericwarningSleepTime->Value;
		}
		System::Void numericwarningWakeTime_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
			warningWakeTime = (int)this->numericwarningWakeTime->Value;
		}
		System::Void buttonUserInfo_Click(System::Object^ sender, System::EventArgs^e) {
			if (!fileOpened) {
				this->userForm = gcnew Capture1::UserForm();
			}
			else if (fileOpened) {
				userName = "";
				userAge = 0;
				userMale = false;
				userDate = "";
				dataFilePath = NULL;
				userSession = false;
				tempFile.close();
				fileOpened = false;
				this->buttonUserInfo->Text = "Add";
				this->textBoxFilePath->Text = "";
				this->buttonRecord->Enabled = false;
				this->buttonRecord->Text = "Record";
				record = false;
			}
		}
		System::Void buttonDiagnose_Click(System::Object^  sender, System::EventArgs^  e) {
			this->diagnoseForm = gcnew Capture1::DiagnoseForm();
		}
	};
}
void captureFrame()
{
	m_capture->operator >> (frame);
	//std::cout << frame.cols << "   " << frame.rows << std::endl;
	if (frame.empty()) {
		fprintf(stderr, "ERROR: Camera is not working");
	}
	cv::flip(frame, frame, 1);
	frame.copyTo(drawn);
	cv::cvtColor(frame, gray, CV_BGR2GRAY);
}
void detectFace()
{
	cv::Mat tmp;
	int resizeWidth = gray.cols * scale_face;
	int resizeHeight = gray.rows * scale_face;
	cv::resize(gray, tmp, cv::Size(resizeWidth, resizeHeight));
	//imshow("Face", tmp);
	m_faceDetector->detectMultiScale(tmp, faces, scaleFactor, minNeighbors, flagsFace, minSize);
	if (faces.size() > 0) {
		faceFound = true;
		for (int i = 0; i < faces.size(); i++) {
			cv::Rect smallFaceRect = faces[i];
			cv::Rect faceRect = cv::Rect(smallFaceRect.x / scale_face + deFaceW, smallFaceRect.y / scale_face + deFaceH, smallFaceRect.width / scale_face - 2 * deFaceW, smallFaceRect.height / scale_face - 2 * deFaceH);
			cv::Rect eyeRegion = cv::Rect(faceRect.x + faceRect.width*eyeRectXRatio, faceRect.y + faceRect.height*eyeRectYRatio, faceRect.width*eyeRectWRatio, faceRect.height*eyeRectHRatio);
			cv::rectangle(drawn, faceRect, cv::Scalar(0, 255, 0), 2);
			FRW = faceRect.width;
			//cv::rectangle(drawn, eyeRegion, cv::Scalar(0, 255, 0), 2);
			eyeROI = drawn(eyeRegion);
			eyeROI.copyTo(grayEye);
		}
	}
}
void detectEye()
{
	cv::Mat tmp;
	char buffer[100];
	//imshow("Color", grayEye);
	int resizeWidth = grayEye.cols * scale_eye;
	int resizeHeight = grayEye.rows * scale_eye;
	cv::resize(grayEye, tmp, cv::Size(resizeWidth, resizeHeight));
	cv::cvtColor(tmp, tmp, CV_BGR2GRAY);
	equalizeHist(tmp, tmp);
	separateHist(tmp);
	//blur(tmp, tmp, cv::Size(5,5), cv::Point(3, 3));
	//imshow("Final", tmp);
	m_eyeDetector->detectMultiScale(tmp, eyes, scaleFactor, minNeighbors, flagsEye, minSize);
	if (eyes.size() > 0) {
		eyeFound = true;
		//sprintf(buffer, "EYE OPEN");
		//cv::putText(drawn, buffer, cv::Point(drawn.cols - 200, 40), 1, CV_FONT_HERSHEY_DUPLEX, cv::Scalar(255, 0, 0), 2);
		for (int i = 0; i < eyes.size(); i++) {
			cv::Rect smalleyeRect = eyes[i];
			cv::Rect eyeRect = cv::Rect(smalleyeRect.x / scale_eye, smalleyeRect.y / scale_eye, smalleyeRect.width / scale_eye, smalleyeRect.height / scale_eye);
			cv::rectangle(eyeROI, eyeRect, cv::Scalar(255, 0, 0), 2);
		}
	}
	//else {
	//	sprintf(buffer, "EYE CLOSE");
	//	cv::putText(drawn, buffer, cv::Point(drawn.cols - 200, 40), 1, CV_FONT_HERSHEY_DUPLEX, cv::Scalar(0, 0, 255), 2);
	//}
}
double avgIntensity(cv::Mat eyeR)
{
	int sum = 0;
	int sumVal = 0;
	double avg;
	for (int i = 0; i < eyeR.rows; i++)
	{
		for (int j = 0; j < eyeR.cols; j++)
		{
			uchar val = eyeR.at<uchar>(i, j);
			int intVal = (int)val;
			sum = sum + 1;
			sumVal = sumVal + intVal;
			avg = sumVal / sum;
		}
	}
	return avg;
}
void resetVar()
{
	faces.clear();
	eyes.clear();
	faceFound = false;
	eyeFound = false;
	frame.release();
	drawn.release();
	gray.release();
	grayEye.release();
	eyeROI.release();
}
void separateHist(cv::Mat &faceImg)
{
	int w = faceImg.cols;
	int h = faceImg.rows;
	Mat wholeFace;
	equalizeHist(faceImg, wholeFace);
	int midX = w / 2;
	Mat leftSide = faceImg(Rect(0, 0, midX, h));
	Mat rightSide = faceImg(Rect(midX, 0, w - midX, h));
	equalizeHist(leftSide, leftSide);
	equalizeHist(rightSide, rightSide);
	for (int y = 0; y<h; y++) {
		for (int x = 0; x<w; x++) {
			int v;
			if (x < w / 4) {
				// Left 25%: just use the left face.
				v = leftSide.at<uchar>(y, x);
			}
			else if (x < w * 2 / 4) {
				// Mid-left 25%: blend the left face & whole face.
				int lv = leftSide.at<uchar>(y, x);
				int wv = wholeFace.at<uchar>(y, x);
				// Blend more of the whole face as it moves
				// further right along the face.
				float f = (x - w * 1 / 4) / (float)(w / 4);
				v = cvRound((1.0f - f) * lv + (f)* wv);
			}
			else if (x < w * 3 / 4) {
				// Mid-right 25%: blend right face & whole face.
				int rv = rightSide.at<uchar>(y, x - midX);
				int wv = wholeFace.at<uchar>(y, x);
				// Blend more of the right-side face as it moves
				// further right along the face.
				float f = (x - w * 2 / 4) / (float)(w / 4);
				v = cvRound((1.0f - f) * wv + (f)* rv);
			}
			else {
				// Right 25%: just use the right face.
				v = rightSide.at<uchar>(y, x - midX);
			}
			faceImg.at<uchar>(y, x) = v;
		}// end x loop
	}//end y loop
}
double diffclock(clock_t clock1, clock_t clock2)
{
	double diffticks = clock1 - clock2;
	double diffms = (diffticks) / (CLOCKS_PER_SEC / 1000);
	return diffms;
}