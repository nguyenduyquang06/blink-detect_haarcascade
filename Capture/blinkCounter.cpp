#include "blinkCounter.h"
blinkCounter::blinkCounter(detectFnc &detector)
{
	m_detector = &detector;
	fps = 0;
	dispFPS = false;
	dispTime = false;
}
blinkCounter::~blinkCounter()
{

}
void blinkCounter::run()
{
	frameCounter = 0;
	tick = 0;
	start = time(0);
	while (1)
	{
		// Main program
		m_detector->captureFrame();
		m_detector->detectFace();
		if (m_detector->faceFound) {
			m_detector->detectEye();
			if (eyeOpen && !m_detector->eyeFound) blinkCount++;
			eyeOpen = m_detector->eyeFound;
			if (eyeOpen) {
				initation = tick;
			}
			else {
				fin = tick;
				if (fin - initation >= warningTime && tick >= warningEnd && alarm) {
					warningEnd = tick + voiceDuration;
					std::cout << "Warning! ----------------- Warning! " << std::endl;
					PlaySound(TEXT("alarm.wav"), NULL, 0x20000 | 1 | 1);
				}
				else if (fin - initation >= warningTime && tick < warningEnd && alarm) {
					std::cout << "Warning! ----------------- Warning! " << std::endl;
					//PlaySound(TEXT("buzzer_x.wav"), NULL, 0x20000 | 1 | 1);
				}
			}
 		}
		std::cout << m_detector->faceFound << std::endl;
		std::cout << m_detector->eyeFound << std::endl << std::endl;
		m_detector->drawn.copyTo(result);
		/* Time */ time(&rawtime);
		addText();
		cv::imshow("Result", result);
		m_detector->resetVar();

		// FPS Calculate
		frameCounter++;
		dur = time(0) - start;
		if (dur - tick >= 1)
		{
			tick++;
			fps = frameCounter;
			frameCounter = 0;
		}
		std::cout << tick << " :::  " << initation << "  " << fin << std::endl;
		int c = cv::waitKey(5);
		if (c == 27) break;
		if (c == 102) toogleDispFPS();
		if (c == 116) toogleDispTime();
		if (c == 114) toogleDispCounter();
		if (c == 97) toogleAlarm();
	}
}
void blinkCounter::toogleDispFPS()
{
	dispFPS = !dispFPS;
}
void blinkCounter::toogleDispTime()
{
	dispTime = !dispTime;
}
void blinkCounter::addText()
{
	if (result.empty()) {
		fprintf(stderr, "ERROR: Result frame is empty\n");
		return;
	}
	char buffer[100];
	if (counting) {
		sprintf(buffer, "BLINK COUNTER: %d", blinkCount);
		cv::putText(result, buffer, cv::Point(5, 35), CV_FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(0, 255, 0), 2);
	}
	if (dispFPS) {
		sprintf(buffer, "FPS: %d", fps);
		cv::putText(result, buffer, cv::Point(5,result.rows -40), CV_FONT_HERSHEY_SIMPLEX, 0.8, cv::Scalar(255, 0, 0),2);
	}
	if (dispTime) {
		sprintf(buffer, "%s", ctime(&rawtime));
		cv::putText(result, buffer, cv::Point(5,result.rows - 5), CV_FONT_HERSHEY_SIMPLEX, 0.8, cv::Scalar(255, 0, 0),2);
	}
	if (alarm) { sprintf(buffer, "Alarm on"); }
	else { sprintf(buffer, "Alarm off"); }
	cv::putText(result, buffer, cv::Point(result.cols -150, result.rows - 5), CV_FONT_HERSHEY_SIMPLEX, 0.8, cv::Scalar(255, 0, 0), 2);
	return;
}
void blinkCounter::toogleDispCounter()
{
	counting = !counting;
	blinkCount = 0;
}
void blinkCounter::toogleAlarm()
{
	alarm = !alarm;
}